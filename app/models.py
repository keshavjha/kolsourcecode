# models.py
import os
from app import db
import psycopg2

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.BigInteger, primary_key=True)
    tenantid = db.Column(db.Integer)
    username = db.Column(db.String(200), unique=True)
    password = db.Column(db.String(50))
    role = db.Column(db.String(50))
    active = db.Column(db.Boolean)
    time_created = db.Column(db.DateTime)
    time_updated = db.Column(db.DateTime)
    emailid = db.Column(db.String(200), unique=True)
    contact = db.Column(db.Numeric(10), unique=True)
    address = db.Column(db.String(200))
    country = db.Column(db.String(30))

    def __init__(self, tenantid, username, password, role, active, time_created, time_updated, emailid, contact, address, country):
        self.tenantid = tenantid
        self.username = username
        self.password = password
        self.role = role
        self.active = active
        self.time_created = time_created
        self.time_updated = time_updated
        self.emailid = emailid
        self.contact = contact
        self.address = address
        self.country = country

class Tenant(db.Model):
    __tablename__ = 'tenant'
    tenantid = db.Column(db.Integer, unique = True, primary_key=True)
    organization_name = db.Column(db.String(200), unique=True)
    realm = db.Column(db.String(20), unique=True)
    locked = db.Column(db.Boolean)
    time_created = db.Column(db.DateTime)
    time_updated = db.Column(db.DateTime)
    contact = db.Column(db.Numeric(10), unique=True)
    address = db.Column(db.String(200))
    country = db.Column(db.String(30))

    def __init__(self, organization_name, realm, locked, time_created, time_updated, contact, address, country):
        self.organization_name = organization_name
        self.realm = realm
        self.locked = locked
        self.time_created = time_created
        self.time_updated = time_updated
        self.contact = contact
        self.address = address
        self.country = country

class UserRequest(db.Model):
    __tablename__ = 'userrequest'
    id = db.Column(db.BigInteger, primary_key=True)
    tenantid = db.Column(db.Integer)
    userid = db.Column(db.Integer)
    requesttype = db.Column(db.String(50))
    status = db.Column(db.String(20))
    uuidno = db.Column(db.String(50))
    requestdate = db.Column(db.DateTime)

    def __init__(self, tenantid, userid, requesttype, status, uuidno, requestdate):
        self.tenantid = tenantid
        self.userid = userid
        self.requesttype = requesttype
        self.status = status
        self.uuidno = uuidno
        self.requestdate = requestdate

# conn = psycopg2.connect("dbname='postgres' user='postgres' host='localhost' password='1234'")
conn = psycopg2.connect("dbname='kolprod' user='postgres' host='flask-app-databasebackend.cgmlyvc8kk7s.eu-central-1.rds.amazonaws.com' password='glocalmind2024'")
cur = conn.cursor()
try:
    cur.execute("select * from tenant")
    table = "yes"
except:
    table = "no"
if table == "yes":
    print("table already exists")
else:
    print("create table")
    print(db.create_all())
    ################################################################ 1 Row User and Tenant #############################################
    tenantid = 1
    organization_name = 'glocalmind'
    realm = 'GM'
    locked = False
    time_created = '2020-04-30 03:45:20'
    time_updated = '2020-05-12 03:48:24'
    contact = 9588823456
    address = '10, Wilson Garden, Benagaluru, Karnataka'
    country = 'India'

    username = 'admin'
    password = 'admin'
    role = 'Admin'
    active = True
    emailid = 'admin@gm.com'


    data_tenant = Tenant(organization_name, realm, locked, time_created, time_updated, contact, address, country)
    db.session.add(data_tenant)
    db.session.commit()

    data_user = User(tenantid, username, password, role, active, time_created, time_updated, emailid, contact, address, country)
    db.session.add(data_user)
    db.session.commit()

    ############################################################### 2 Row User ##############################################################
    tenantid = 1
    time_created = '2020-03-05 12:23:20'
    time_updated = '2020-03-05 13:57:24'
    contact = 9534823456
    address = 'Benagaluru, Karnataka'
    country = 'India'

    username = 'superadmin'
    password = 'superadmin'
    role = 'Superadmin'
    active = True
    emailid = 'superadmin@gm.com'

    data_user = User(tenantid, username, password, role, active, time_created, time_updated, emailid, contact, address, country)
    db.session.add(data_user)
    db.session.commit()

    ############################################################## ROW 3-User and 2-Tenant #########################################################
    tenantid = 2
    organization_name = 'SAP Labs'
    realm = 'SAP'
    locked = False
    time_created = '2020-05-05 05:23:20'
    time_updated = '2020-05-05 13:57:24'
    contact = 2224823456
    address = '10, Saint Church, LA'
    country = 'U.S.A.'

    username = 'adminsap'
    password = 'adminsap'
    role = 'Admin'
    active = True
    emailid = 'admin@sap.com'


    data_tenant = Tenant(organization_name, realm, locked, time_created, time_updated, contact, address, country)
    db.session.add(data_tenant)
    db.session.commit()

    data_user = User(tenantid, username, password, role, active, time_created, time_updated, emailid, contact, address, country)
    db.session.add(data_user)
    db.session.commit()

    ############################################################## Row 4-User ###########################################################
    tenantid = 1
    time_created = '2020-05-05 12:23:20'
    time_updated = '2020-07-05 13:57:24'
    contact = 9534333456
    address = 'Jaipur, Rajasthan'
    country = 'India'

    username = 'aditya'
    password = 'test'
    role = 'Supplier'
    active = True
    emailid = 'aditya@gm.com'

    data_user = User(tenantid, username, password, role, active, time_created, time_updated, emailid, contact, address, country)
    db.session.add(data_user)
    db.session.commit()

    ############################################################### Row 5-User 3-Tenant ########################################################
    tenantid = 3
    organization_name = 'Falcy'
    realm = 'FAL'
    locked = True
    time_created = '2020-06-26 09:23:20'
    time_updated = '2020-07-07 06:57:24'
    contact = 2343533456
    address = 'St. Petersburg, Cambridge'
    country = 'U.K.'

    username = 'falcy'
    password = 'falcy123'
    role = 'Employee'
    active = True
    emailid = 'falcy@gmail.com'


    data_tenant = Tenant(organization_name, realm, locked, time_created, time_updated, contact, address, country)
    db.session.add(data_tenant)
    db.session.commit()

    data_user = User(tenantid, username, password, role, active, time_created, time_updated, emailid, contact, address, country)
    db.session.add(data_user)
    db.session.commit()

    ############################################################## 6 Row User ###########################################################
    tenantid = 2
    time_created = '2020-06-10 12:23:20'
    time_updated = '2020-06-30 13:57:24'
    contact = 9588828456
    address = 'Pune, Maharashtra'
    country = 'India'

    username = 'aditya123'
    password = 'test123'
    role = 'Employee'
    active = False
    emailid = 'aditya@sap.com'

    data_user = User(tenantid, username, password, role, active, time_created, time_updated, emailid, contact, address, country)
    db.session.add(data_user)
    db.session.commit()


    ####################################################################################################################################
