from flask import *
from flask_sqlalchemy import SQLAlchemy
import uuid
from subprocess import run,PIPE,Popen
from datetime import datetime
#
# from pandas.tests.tseries.offsets.test_fiscal import test_get_offset_name
from pytz import timezone
import os, sys, shutil, json

import requests
import xmltodict, xlsxwriter
import openpyxl
from clinical_trials import Trials
import zipfile

# from concurrent.futures import ThreadPoolExecutor

import boto3
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')

s3_client = boto3.client(
    's3',
    'us-west-2',
    aws_access_key_id=AWS_ACCESS_KEY_ID,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY
    )

s3_resource = boto3.resource('s3',
    aws_access_key_id=AWS_ACCESS_KEY_ID,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY
    )

bucket_name = "glocalmindkoldata"
database_uri = 'postgresql+psycopg2://{dbuser}:{dbpass}@{dbhost}/{dbname}'.format(
    dbuser='postgres',
    dbpass='glocalmind2024',
    dbhost='flask-app-databasebackend.cgmlyvc8kk7s.eu-central-1.rds.amazonaws.com',
    # dbhost='localhost',
    dbname='kolprod'
)

app = Flask(__name__)
app.debug = True
app.config.update(
    SQLALCHEMY_DATABASE_URI=database_uri,
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
)

# initialize the database connection
db = SQLAlchemy(app)

from models import *

# migrate = Migrate(app, db)

# initialize database migration management

def UserQuery(string, username, status, requesttype, page_num):
    if status == "In Progress":
        db_query_user = db.session.query(User)
        query_filter = db_query_user.filter(User.username == username)
        for i in query_filter:
            tenantid = i.tenantid
            userid = i.id

        format = "%Y-%m-%d %H:%M:%S"
        now_utc = datetime.now(timezone('UTC'))
        now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
        requestdate = now_asia.strftime(format)
        requesttype = requesttype
        status = status
        uuidno = string
        data_userreq = UserRequest(tenantid, userid, requesttype, status, uuidno, requestdate)
        db.session.add(data_userreq)
        db.session.commit()
        return userid

    else:
        db_query_user = db.session.query(User)
        query_user_filter = db_query_user.filter(User.username == username)
        userid = ''
        for i in query_user_filter:
            userid = i.id
        db_query_userreq = db.session.query(UserRequest)
        query_userreq_filter = db_query_userreq.filter(UserRequest.uuidno == string).update({'status':status},synchronize_session='fetch')
        db.session.commit()
        print("updated to database")
        return userid

@app.route('/')
def login_page():
    return render_template("New-login-KOL.html")

@app.route('/submit/<int:page_num>', methods=['POST'])
def submit(page_num = 1):
    global username
    global role
    if request.method == 'POST':
        realm = request.form['realm']
        emailid = request.form['id']
        password = request.form['password']

        if emailid == '' or password == '' or realm == '':
            return render_template('New-login-KOL.html', message = 'Please enter required fields')
        else:
            db_query_tenant = db.session.query(Tenant)
            query_tenant_filter = db_query_tenant.filter(Tenant.realm == realm)
            for a in query_tenant_filter:
                tenantid = a.tenantid
                locked = a.locked

            if locked == True:
                return render_template('New-login-KOL.html', message='Your account is blocked, kindly contact with administrator!')
            else:
                db_query_user = db.session.query(User)
                query_user_filter = db_query_user.filter(User.emailid == emailid, User.password == password, User.tenantid == tenantid)
                if query_user_filter.count()!=0:
                    for q in query_user_filter:
                        username = q.username
                        role = q.role
                        active = q.active
                    if active == False:
                        return render_template('New-login-KOL.html', message='Your account status is in-active. Kindly contact with administrator!')
                    else:
                        if role == 'Superadmin':
                            home_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)

                            db_query_userreq = db.session.query(UserRequest)
                            kol_rank_raw_count = (db_query_userreq.filter(UserRequest.requesttype == "Raw")).count()
                            kol_rank_profiling_count = (db_query_userreq.filter(UserRequest.requesttype == "Profiling")).count()
                            kol_rank_count = kol_rank_raw_count + kol_rank_profiling_count
                            twitter_count = (db_query_userreq.filter(UserRequest.requesttype == "Twitter")).count()
                            crawler_count = (db_query_userreq.filter(UserRequest.requesttype == "Crawler")).count()
                            pubmed_count = (db_query_userreq.filter(UserRequest.requesttype == "Pubmed and Clinical Trails")).count()

                            if kol_rank_count == 0:
                                kol_rank_count = '0'
                            if twitter_count == 0:
                                twitter_count = '0'
                            if crawler_count == 0:
                                crawler_count = '0'
                            if pubmed_count == 0:
                                pubmed_count = '0'
                            return render_template("home.html", username=username, home_list=home_list, kol_rank_count=kol_rank_count, twitter_count=twitter_count, crawler_count=crawler_count, pubmed_count = pubmed_count, role=role)

                        elif role == "Admin":
                            db_query_user = db.session.query(User)
                            query_filter = db_query_user.filter(User.username == username)
                            for i in query_filter:
                                tenantid = i.tenantid

                            home_json_list = UserRequest.query.filter(UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)

                            db_query_userreq = db.session.query(UserRequest)
                            kol_rank_raw_count = (db_query_userreq.filter(UserRequest.tenantid == tenantid, UserRequest.requesttype == "Raw")).count()
                            kol_rank_profiling_count = (db_query_userreq.filter(UserRequest.tenantid == tenantid, UserRequest.requesttype == "Profiling")).count()
                            kol_rank_count = kol_rank_raw_count + kol_rank_profiling_count
                            twitter_count = (db_query_userreq.filter(UserRequest.tenantid == tenantid, UserRequest.requesttype == "Twitter")).count()
                            crawler_count = (db_query_userreq.filter(UserRequest.tenantid == tenantid, UserRequest.requesttype == "Crawler")).count()
                            pubmed_count = (db_query_userreq.filter(UserRequest.tenantid == tenantid, UserRequest.requesttype == "Pubmed and Clinical Trails")).count()

                            if kol_rank_count == 0:
                                kol_rank_count = '0'
                            if twitter_count == 0:
                                twitter_count = '0'
                            if crawler_count == 0:
                                crawler_count = '0'
                            if pubmed_count == 0:
                                pubmed_count = '0'
                            return render_template("home.html", username=username, home_json_list=home_json_list, kol_rank_count=kol_rank_count, twitter_count=twitter_count, pubmed_count= pubmed_count, crawler_count=crawler_count, role=role)


                        else:
                            query_filter = db_query_user.filter(User.username == username)
                            for i in query_filter:
                                userid = i.id

                            home_json_list = UserRequest.query.filter(UserRequest.userid == userid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)

                            db_query_userreq = db.session.query(UserRequest)
                            kol_rank_raw_count = (db_query_userreq.filter(UserRequest.userid == userid, UserRequest.requesttype == "Raw")).count()
                            kol_rank_profiling_count = (db_query_userreq.filter(UserRequest.userid == userid, UserRequest.requesttype == "Profiling")).count()
                            kol_rank_count = kol_rank_raw_count + kol_rank_profiling_count
                            twitter_count = (db_query_userreq.filter(UserRequest.userid == userid, UserRequest.requesttype == "Twitter")).count()
                            crawler_count = (db_query_userreq.filter(UserRequest.userid == userid, UserRequest.requesttype == "Crawler")).count()
                            pubmed_count = (db_query_userreq.filter(UserRequest.userid == userid, UserRequest.requesttype == "Pubmed and Clinical Trails")).count()

                            if kol_rank_count == 0:
                                kol_rank_count = '0'
                            if twitter_count == 0:
                                twitter_count = '0'
                            if crawler_count == 0:
                                crawler_count = '0'
                            if pubmed_count == 0:
                                pubmed_count = '0'

                            return render_template("home.html", username=username, home_json_list=home_json_list, kol_rank_count=kol_rank_count, twitter_count=twitter_count, crawler_count=crawler_count, pubmed_count=pubmed_count, role=role)
                else:
                    return render_template('New-login-KOL.html', message='invalid credentials')

@app.route('/home/<int:page_num>')
def home(page_num):
    if role == "Superadmin":
        home_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)

        db_query_userreq = db.session.query(UserRequest)
        kol_rank_raw_count = (db_query_userreq.filter(UserRequest.requesttype == "Raw")).count()
        kol_rank_profiling_count = (db_query_userreq.filter(UserRequest.requesttype == "Profiling")).count()
        kol_rank_count = kol_rank_raw_count + kol_rank_profiling_count
        twitter_count = (db_query_userreq.filter(UserRequest.requesttype == "Twitter")).count()
        crawler_count = (db_query_userreq.filter(UserRequest.requesttype == "Crawler")).count()
        pubmed_count = (db_query_userreq.filter(UserRequest.requesttype == "Pubmed and Clinical Trails")).count()

        if kol_rank_count == 0:
            kol_rank_count = '0'
        if twitter_count == 0:
            twitter_count = '0'
        if crawler_count == 0:
            crawler_count = '0'
        if pubmed_count == 0:
            pubmed_count = '0'

        return render_template("home.html", username=username, home_list=home_list, kol_rank_count=kol_rank_count, twitter_count=twitter_count, crawler_count=crawler_count, pubmed_count=pubmed_count, role=role)

    elif role == "Admin":
        db_query_user = db.session.query(User)
        query_filter = db_query_user.filter(User.username == username)
        for i in query_filter:
            tenantid = i.tenantid

        home_json_list = UserRequest.query.filter(UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)

        db_query_userreq = db.session.query(UserRequest)
        kol_rank_raw_count = (db_query_userreq.filter(UserRequest.tenantid == tenantid, UserRequest.requesttype == "Raw")).count()
        kol_rank_profiling_count = (db_query_userreq.filter(UserRequest.tenantid == tenantid, UserRequest.requesttype == "Profiling")).count()
        kol_rank_count = kol_rank_raw_count + kol_rank_profiling_count
        twitter_count = (db_query_userreq.filter(UserRequest.tenantid == tenantid, UserRequest.requesttype == "Twitter")).count()
        crawler_count = (db_query_userreq.filter(UserRequest.tenantid == tenantid, UserRequest.requesttype == "Crawler")).count()
        pubmed_count = (db_query_userreq.filter(UserRequest.tenantid == tenantid, UserRequest.requesttype == "Pubmed and Clinical Trails")).count()

        if kol_rank_count == 0:
            kol_rank_count = '0'
        if twitter_count == 0:
            twitter_count = '0'
        if crawler_count == 0:
            crawler_count = '0'
        if pubmed_count == 0:
            pubmed_count = '0'
        return render_template("home.html", username=username, home_json_list=home_json_list, kol_rank_count=kol_rank_count, twitter_count=twitter_count, pubmed_count= pubmed_count, crawler_count=crawler_count, role=role)

    else:
        db_query_user = db.session.query(User)
        query_filter = db_query_user.filter(User.username == username)
        for i in query_filter:
            userid = i.id

        home_json_list = UserRequest.query.filter(UserRequest.userid == userid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)

        db_query_userreq = db.session.query(UserRequest)
        kol_rank_raw_count = (db_query_userreq.filter(UserRequest.userid == userid, UserRequest.requesttype == "Raw")).count()
        kol_rank_profiling_count = (db_query_userreq.filter(UserRequest.userid == userid, UserRequest.requesttype == "Profiling")).count()
        kol_rank_count = kol_rank_raw_count + kol_rank_profiling_count
        twitter_count = (db_query_userreq.filter(UserRequest.userid == userid, UserRequest.requesttype == "Twitter")).count()
        crawler_count = (db_query_userreq.filter(UserRequest.userid == userid, UserRequest.requesttype == "Crawler")).count()
        pubmed_count = (db_query_userreq.filter(UserRequest.userid == userid, UserRequest.requesttype == "Pubmed and Clinical Trails")).count()

        if kol_rank_count == 0:
            kol_rank_count = '0'
        if twitter_count == 0:
            twitter_count = '0'
        if crawler_count == 0:
            crawler_count = '0'
        if pubmed_count == 0:
            pubmed_count = '0'
        return render_template("home.html", username=username, home_json_list=home_json_list, kol_rank_count=kol_rank_count, twitter_count=twitter_count, pubmed_count= pubmed_count, crawler_count=crawler_count, role=role)

@app.route('/kol_ranking_tool/<int:page_num>')
def kol_ranking_tool(page_num):
    db_query_user = db.session.query(User)
    query_filter = db_query_user.filter(User.username == username)
    requesttype = "Raw"
    for i in query_filter:
        userid = i.id
        tenantid = i.tenantid
    if role == 'Superadmin':
        all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("kol-ranking.html", username=username, all_json_list=all_json_list, role=role)
    elif role == "Admin":
        all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype, UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("kol-ranking.html", username=username, all_json_list=all_json_list, role=role)
    else:
        json_list = UserRequest.query.filter(UserRequest.userid == userid, UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("kol-ranking.html", username=username, json_list=json_list, role=role)

@app.route('/twitter_tool/<int:page_num>')
def twitter_tool(page_num):
    db_query_user = db.session.query(User)
    query_filter = db_query_user.filter(User.username == username)
    requesttype = "Twitter"
    for i in query_filter:
        userid = i.id
        tenantid = i.tenantid
    if role == 'Superadmin':
        all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("twitter_tool.html", username=username, all_json_list=all_json_list, role=role)
    elif role == "Admin":
        all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype, UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("twitter_tool.html", username=username, all_json_list=all_json_list, role=role)
    else:
        json_list = UserRequest.query.filter(UserRequest.userid == userid, UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("twitter_tool.html", username=username, json_list=json_list, role=role)

@app.route('/crawler_tool/<int:page_num>')
def crawler_tool(page_num):
    db_query_user = db.session.query(User)
    query_filter = db_query_user.filter(User.username == username)
    requesttype = "Crawler"
    for i in query_filter:
        userid = i.id
        tenantid = i.tenantid
    if role == 'Superadmin':
        all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("crawler_tool.html", username=username, all_json_list=all_json_list, role=role)
    elif role == "Admin":
        all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype, UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("crawler_tool.html", username=username, all_json_list=all_json_list, role=role)
    else:
        json_list = UserRequest.query.filter(UserRequest.userid == userid, UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("crawler_tool.html", username=username, json_list=json_list, role=role)

@app.route('/pubmed_clinical_tool/<int:page_num>')
def pubmed_clinical_tool(page_num):
    db_query_user = db.session.query(User)
    query_filter = db_query_user.filter(User.username == username)
    requesttype = "Pubmed and Clinical Trails"
    for i in query_filter:
        userid = i.id
        tenantid = i.tenantid
    if role == 'Superadmin':
        all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("pubmed_clinical_data.html", username=username, all_json_list=all_json_list, role=role)
    elif role == "Admin":
        all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype, UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("pubmed_clinical_data.html", username=username, all_json_list=all_json_list, role=role)
    else:
        json_list = UserRequest.query.filter(UserRequest.userid == userid, UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("pubmed_clinical_data.html", username=username, json_list=json_list, role=role)

@app.route('/user_management/<int:page_num>')
def user_management(page_num):
    if role == 'Superadmin':
        all_users = User.query.join(Tenant, User.tenantid==Tenant.tenantid).add_columns(User.username, User.password, User.role, User.active, User.emailid, User.contact, User.address, User.country, Tenant.organization_name).order_by(User.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("user_management.html", username=username, all_users=all_users, role=role)
    else:
        db_query_user = db.session.query(User)
        query_user_filter = db_query_user.filter(User.username == username)
        for i in query_user_filter:
            tenantid = i.tenantid
        users = User.query.filter(User.tenantid == tenantid).order_by(User.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("user_management.html", username=username, users=users, role=role)

@app.route('/client_management/<int:page_num>')
def client_management(page_num):
    all_tenant = Tenant.query.order_by(Tenant.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
    return render_template("client_management.html", username=username, all_tenant=all_tenant, role=role)

@app.route('/update_profile')
def update_profile():
    return render_template("update_profile.html", username=username, role=role)

@app.route('/add_edit_user')
def add_edit_user():
    return render_template("add_user.html", username=username, role=role)

@app.route('/add_edit_client')
def add_edit_client():
    return render_template("add_client.html", username=username, role=role)
#############################################        KOL Tool(Ranking)     ################################################################

@app.route('/run_profiling_ranking/<int:page_num>', methods=['POST'])
def run_profiling_ranking(page_num):
    global string2
    if request.method == 'POST':
        myfile = request.files['myfile']
        file = request.files['file']
        result = uuid.uuid4()
        string2 = result.hex
        status = "In Progress"
        requesttype = "Profiling"

        os.mkdir("uploads/media/%s" % string2)
        os.mkdir("uploads/media/%s/input_folder" % string2)
        os.mkdir("uploads/media/%s/output_folder" % string2)
        file_folder =  "uploads/media/%s/input_folder" % (string2)

        myfile.save(os.path.join(file_folder, myfile.filename))
        file.save(os.path.join(file_folder, file.filename))

        if os.getenv('FLASK_ENV')=="production":
            s3_resource.meta.client.upload_file("uploads/media/%s/input_folder/%s" %(string2, myfile.filename), bucket_name, '%s/input_folder/%s' %(string2, myfile.filename))
            s3_resource.meta.client.upload_file("uploads/media/%s/input_folder/%s" %(string2, file.filename), bucket_name, '%s/input_folder/%s' %(string2, file.filename))

        a = UserQuery(string2, username, status, requesttype, page_num)
        out2=run([sys.executable, "uploads/media/kol_profiling_ranking.py", string2], shell=False, stdout=PIPE)

        if os.getenv('FLASK_ENV')=="production":
            s3_resource.meta.client.upload_file("uploads/media/%s/output_folder/profiling_output_rank.xlsx" %(string2), bucket_name, '%s/output_folder/%s' %(string2,'profiling_output_rank.xlsx'))
            if os.path.exists("uploads/media/%s" %string2):
                try:
                    shutil.rmtree("uploads/media/%s" %string2, ignore_errors=True)
                    print("deleted")
                except OSError as e:
                    print ("Error: %s - %s." % (e.filename, e.strerror))

        status = "Completed"
        requesttype = "Profiling"

        userid = UserQuery(string2, username, status, requesttype, page_num)
        db_query_user = db.session.query(User)
        query_filter = db_query_user.filter(User.username == username)
        for i in query_filter:
            tenantid = i.tenantid

        if role == 'Superadmin':
            all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("kol-ranking.html", username=username, all_json_list=all_json_list, role=role)
        elif role == "Admin":
            all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype, UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("kol-ranking.html", username=username, all_json_list=all_json_list, role=role)
        else:
            json_list = UserRequest.query.filter(UserRequest.userid == userid, UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("kol-ranking.html", username=username, json_list=json_list, role=role)

@app.route('/run_rawdata_ranking/<int:page_num>', methods=['POST'])
def run_rawdata_ranking(page_num):
    global string1
    if request.method == 'POST':
        myfile = request.files['myfile']
        file = request.files['file']
        result = uuid.uuid4()
        string1 = result.hex
        status = "In Progress"
        requesttype = "Raw"

        os.mkdir("uploads/media/%s" % string1)
        os.mkdir("uploads/media/%s/input_folder" % string1)
        os.mkdir("uploads/media/%s/output_folder" % string1)
        file_folder =  "uploads/media/%s/input_folder" % (string1)

        myfile.save(os.path.join(file_folder, myfile.filename))
        file.save(os.path.join(file_folder, file.filename))

        if os.getenv('FLASK_ENV')=="production":
            s3_resource.meta.client.upload_file("uploads/media/%s/input_folder/%s" %(string1, myfile.filename), bucket_name, '%s/input_folder/%s' %(string1, myfile.filename))
            s3_resource.meta.client.upload_file("uploads/media/%s/input_folder/%s" %(string1, file.filename), bucket_name, '%s/input_folder/%s' %(string1, file.filename))

        a = UserQuery(string1, username, status, requesttype, page_num)
        out2=run([sys.executable, "uploads/media/kol_rawdata_ranking.py", string1], shell=False, stdout=PIPE)

        if os.getenv('FLASK_ENV')=="production":
            s3_resource.meta.client.upload_file("uploads/media/%s/output_folder/rawdata_output_rank.xlsx" %(string1), bucket_name, '%s/output_folder/%s' %(string1,'rawdata_output_rank.xlsx'))
            if os.path.exists("uploads/media/%s" %string1):
                try:
                    shutil.rmtree("uploads/media/%s" %string1, ignore_errors=True)
                    print("deleted")
                except OSError as e:
                    print ("Error: %s - %s." % (e.filename, e.strerror))

        status = "Completed"
        requesttype = "Raw"

        userid = UserQuery(string1, username, status, requesttype, page_num)
        db_query_user = db.session.query(User)
        query_filter = db_query_user.filter(User.username == username)
        for i in query_filter:
            tenantid = i.tenantid

        if role == 'Superadmin':
            all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("kol-ranking.html", username=username, all_json_list=all_json_list, role=role)
        elif role == "Admin":
            all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype, UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("kol-ranking.html", username=username, all_json_list=all_json_list, role=role)
        else:
            json_list = UserRequest.query.filter(UserRequest.userid == userid, UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("kol-ranking.html", username=username, json_list=json_list, role=role)

@app.route('/inputprofiling_txtfile')
def inputprofiling_txtfile():
    path = "uploads/media/input_profilingdata_format.txt"
    return send_file(path, as_attachment=True)

@app.route('/inputrawdata_txtfile')
def inputrawdata_txtfile():
    path = "uploads/media/input_rawdata_format.txt"
    return send_file(path, as_attachment=True)

@app.route('/inputprofiling_xlfile')
def inputprofiling_xlfile():
    path = "uploads/media/profiling_data_format.xlsx"
    return send_file(path, as_attachment=True)

@app.route('/inputrawdata_xlfile')
def inputrawdata_xlfile():
    path = "uploads/media/rawdata_format.xlsx"
    return send_file(path, as_attachment=True)

@app.route('/profiling_output', methods=['POST'])
def outputprofiling_output():
    profiling_output = request.form.get("uuidno_output")
    if os.getenv('FLASK_ENV')=="production":
        file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/output_folder/profiling_output_rank.xlsx' %profiling_output)
        return Response(
            file['Body'].read(),
            mimetype='text/plain',
            headers={"Content-Disposition": "attachment;filename=profiling_output_rank.xlsx"}
        )
    else:
        path = "uploads/media/%s/output_folder/profiling_output_rank.xlsx" %profiling_output
        return send_file(path, as_attachment=True)


@app.route('/rawdata_output', methods=['POST'])
def outputrawdata_output():
    raw_output = request.form.get("uuidno_output")
    if os.getenv('FLASK_ENV')=="production":
        file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/output_folder/rawdata_output_rank.xlsx' %raw_output)
        return Response(
            file['Body'].read(),
            mimetype='text/plain',
            headers={"Content-Disposition": "attachment;filename=rawdata_output_rank.xlsx"}
        )
    else:
        path = "uploads/media/%s/output_folder/rawdata_output_rank.xlsx" %raw_output
        return send_file(path, as_attachment=True)


@app.route('/profiling_input', methods=['POST'])
def outputprofiling_input():
    profiling_input = request.form.get("uuidno_input")
    if os.getenv('FLASK_ENV')=="production":
        file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/input_folder/profiling_data.xlsx' %profiling_input)
        return Response(
            file['Body'].read(),
            mimetype='text/plain',
            headers={"Content-Disposition": "attachment;filename=profiling_data.xlsx"}
        )
    else:
        path = "uploads/media/%s/input_folder/profiling_data.xlsx" %profiling_input
        return send_file(path, as_attachment=True)


@app.route('/rawdata_input', methods=['POST'])
def outputrawdata_input():
    rawdata_input = request.form.get("uuidno_input")
    if os.getenv('FLASK_ENV')=="production":
        file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/input_folder/raw_data.xlsx' %rawdata_input)
        return Response(
            file['Body'].read(),
            mimetype='text/plain',
            headers={"Content-Disposition": "attachment;filename=raw_data.xlsx"}
        )
    else:
        path = "uploads/media/%s/input_folder/raw_data.xlsx" % rawdata_input
        return send_file(path, as_attachment=True)


#############################################        KOL TOOL(Twitter Tool)     ################################################################

@app.route('/inputtwitter_xlfile')
def inputtwitter_xlfile():
    path = "uploads/media/twitter_data_format.xlsx"
    return send_file(path, as_attachment=True)

@app.route('/run_twitter/<int:page_num>', methods=['POST'])
def run_twitter(page_num):
    global string3
    if request.method == 'POST':
        f = request.files['myfile']
        result = uuid.uuid4()
        string3 = result.hex
        status = "In Progress"
        requesttype = "Twitter"

        startDate = request.form['startDate']  if request.form['startDate'] else "2006-03-21"
        endDate = request.form['endDate']  if request.form['endDate'] else "2036-12-31"

        os.mkdir("uploads/media/%s" % string3)
        os.mkdir("uploads/media/%s/input_folder" % string3)
        os.mkdir("uploads/media/%s/output_folder" % string3)
        os.mkdir("uploads/media/%s/zipfolder" % string3)
        file_folder =  "uploads/media/%s/input_folder" % (string3)
        f.save(os.path.join(file_folder, f.filename))

        if os.getenv('FLASK_ENV')=="production":
            s3_resource.meta.client.upload_file("uploads/media/%s/input_folder/%s" %(string3, f.filename), bucket_name, '%s/input_folder/%s' %(string3, f.filename))

        a = UserQuery(string3, username, status, requesttype, page_num)
        out3=run([sys.executable, "uploads/media/tweet_upload.py", string3, startDate, endDate], shell=False, stdout=PIPE)
        if os.getenv('FLASK_ENV')=="production":
            s3_resource.meta.client.upload_file("uploads/media/%s/zipfolder/tweet.zip" %(string3), bucket_name, '%s/zipfolder/%s' %(string3,'tweet.zip'))
            if os.path.exists("uploads/media/%s" %string3):
                try:
                    shutil.rmtree("uploads/media/%s" %string3, ignore_errors=True)
                    print("deleted")
                except OSError as e:
                    print ("Error: %s - %s." % (e.filename, e.strerror))

        status = "Completed"
        requesttype = "Twitter"
        userid = UserQuery(string3, username, status, requesttype, page_num)
        db_query_user = db.session.query(User)
        query_filter = db_query_user.filter(User.username == username)
        for i in query_filter:
            tenantid = i.tenantid

        if role == 'Superadmin':
            all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("twitter_tool.html", username=username, all_json_list=all_json_list, role=role)
        elif role == "Admin":
            all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype, UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("twitter_tool.html", username=username, all_json_list=all_json_list, role=role)
        else:
            json_list = UserRequest.query.filter(UserRequest.userid == userid, UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("twitter_tool.html", username=username, json_list=json_list, role=role)

@app.route('/twitter_output', methods=['POST'])
def twitter_output():
    tweet_data_output = request.form.get("uuidno_output")
    if os.getenv('FLASK_ENV')=="production":
        file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/zipfolder/tweet.zip'%tweet_data_output)
        return Response(
            file['Body'].read(),
            mimetype='text/plain',
            headers={"Content-Disposition": "attachment;filename=tweet.zip"}
        )
    else:
        path = "uploads/media/%s/zipfolder/tweet.zip" % tweet_data_output
        return send_file(path, as_attachment=True)

@app.route('/twitter_input', methods=['POST'])
def twitter_input():
    tweet_data_input = request.form.get("uuidno_input")
    if os.getenv('FLASK_ENV')=="production":
        my_bucket = s3_resource.Bucket('glocalmindkoldata')
        for object_summary in my_bucket.objects.filter(Prefix="%s/input_folder/" %tweet_data_input):
            print("input file path: ",object_summary.key)
            file_path = object_summary.key
            file_name = file_path.split('/')[1:][-1]
        file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/input_folder/%s' %(tweet_data_input,file_name))
        return Response(
            file['Body'].read(),
            mimetype='text/plain',
            headers={"Content-Disposition": "attachment;filename=input.xlsx"}
        )
    else:
        inputpath = "uploads/media/%s/input_folder" % tweet_data_input
        file_name = ""
        for f in os.listdir(inputpath):
            file_name = f
        path = "uploads/media/%s/input_folder/%s" % (tweet_data_input, file_name)
        return send_file(path, as_attachment=True)


#############################################        KOL TOOL(Crawler Tool)     ################################################################


@app.route('/input_event_searchxl')
def input_event_searchxl():
    path = "uploads/media/event_search.xlsx"
    return send_file(path, as_attachment=True)


@app.route('/run_crawler/<int:page_num>', methods=['POST'])
def run_crawler(page_num):
    global string4
    if request.method == 'POST':
        f = request.files['myfile']
        result = uuid.uuid4()
        string4 = result.hex
        status = "In Progress"
        requesttype = "Crawler"

        os.mkdir("uploads/media/%s" % string4)
        os.mkdir("uploads/media/%s/input_folder" % string4)
        os.mkdir("uploads/media/%s/output_folder" % string4)
        os.mkdir("uploads/media/%s/zipfolder" % string4)
        file_folder =  "uploads/media/%s/input_folder" % (string4)
        f.save(os.path.join(file_folder, f.filename))

        if os.getenv('FLASK_ENV')=="production":
            s3_resource.meta.client.upload_file("uploads/media/%s/input_folder/%s" %(string4, f.filename), bucket_name, '%s/input_folder/%s' %(string4, f.filename))

        a = UserQuery(string4, username, status, requesttype, page_num)
        out4=run([sys.executable, "uploads/media/crawler.py", string4], shell=False, stdout=PIPE)

        if os.getenv('FLASK_ENV')=="production":
            s3_resource.meta.client.upload_file("uploads/media/%s/zipfolder/output.zip" %(string4), bucket_name, '%s/zipfolder/%s' %(string4,'output.zip'))
            if os.path.exists("uploads/media/%s" %string4):
                try:
                    shutil.rmtree("uploads/media/%s" %string4, ignore_errors=True)
                    print("deleted")
                except OSError as e:
                    print ("Error: %s - %s." % (e.filename, e.strerror))

        status = "Completed"
        requesttype = "Crawler"
        userid = UserQuery(string4, username, status, requesttype, page_num)
        db_query_user = db.session.query(User)
        query_filter = db_query_user.filter(User.username == username)
        for i in query_filter:
            tenantid = i.tenantid

        if role == 'Superadmin':
            all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("crawler_tool.html", username=username, all_json_list=all_json_list, role=role)
        elif role == "Admin":
            all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype, UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("crawler_tool.html", username=username, all_json_list=all_json_list, role=role)
        else:
            json_list = UserRequest.query.filter(UserRequest.userid == userid, UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("crawler_tool.html", username=username, json_list=json_list, role=role)

@app.route('/crawler_output', methods=['POST'])
def crawler_output():
    crawler_data_output = request.form.get("uuidno_output")
    if os.getenv('FLASK_ENV')=="production":
        file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/zipfolder/output.zip' %crawler_data_output)
        return Response(
            file['Body'].read(),
            mimetype='text/plain',
            headers={"Content-Disposition": "attachment;filename=output.zip"}
        )
    else:
        path = "uploads/media/%s/zipfolder/output.zip" % crawler_data_output
        return send_file(path, as_attachment=True)

@app.route('/crawler_input', methods=['POST'])
def crawler_input():
    crawler_data_input = request.form.get("uuidno_input")
    if os.getenv('FLASK_ENV')=="production":
        my_bucket = s3_resource.Bucket('glocalmindkoldata')
        for object_summary in my_bucket.objects.filter(Prefix="%s/input_folder/" %crawler_data_input):
            print("input file path: ",object_summary.key)
            file_path = object_summary.key
            file_name = file_path.split('/')[1:][-1]

        file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/input_folder/%s' %(crawler_data_input,file_name))
        return Response(
            file['Body'].read(),
            mimetype='text/plain',
            headers={"Content-Disposition": "attachment;filename=input.xlsx"}
        )
    else:
        inputpath = "uploads/media/%s/input_folder" % crawler_data_input
        file_name = ""
        for f in os.listdir(inputpath):
            file_name = f
        path = "uploads/media/%s/input_folder/%s" % (crawler_data_input, file_name)
        return send_file(path, as_attachment=True)


#############################################        KOL TOOL(Pubmed and Clinical Tool)     ################################################################
import random
temp_path = os.path.dirname(os.path.abspath(__file__)) + os.path.sep + "uploads/media"
if not os.path.exists(temp_path):
    os.makedirs(temp_path)

SHEET_LIMIT = 50000
MAX_SHEETS_PER_XLS = 7
MY_API_KEY = ["f1b6155a866fef706116770dd2f6b6caab08", "54cee9a744a7ed3eafc73842ac5914b09a09", "271a689fa44a52e202c3154c608674cc4208", "5ad1cb607dba294c07b30329863359d45a08", "aeb4b5888f8206876b3d6be7c77b3fc87308"]
PUBMED_SEARCH_URL = ["https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&api_key=f1b6155a866fef706116770dd2f6b6caab08&term={}",
                    "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&api_key=54cee9a744a7ed3eafc73842ac5914b09a09&term={}",
                    "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&api_key=271a689fa44a52e202c3154c608674cc4208&term={}",
                    "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&api_key=5ad1cb607dba294c07b30329863359d45a08&term={}",
                    "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&api_key=aeb4b5888f8206876b3d6be7c77b3fc87308&term={}"
                    ]
PUBMED_DATE_QUERY = '+AND+("{}"[PDat] : "{}"[PDat])'
#PUBMED_DOWNLOAD_CSV =  "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id={}&rettype=fasta&retmode=xml&api_key=6f63b0b5ec41afd50bed862a0d61ff0ae709"
PUBMED_DOWNLOAD_CSV =  ["https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&rettype=fasta&retmode=xml&api_key=f1b6155a866fef706116770dd2f6b6caab08",
                        "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&rettype=fasta&retmode=xml&api_key=54cee9a744a7ed3eafc73842ac5914b09a09",
                        "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&rettype=fasta&retmode=xml&api_key=271a689fa44a52e202c3154c608674cc4208",
                        "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&rettype=fasta&retmode=xml&api_key=5ad1cb607dba294c07b30329863359d45a08",
                        "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&rettype=fasta&retmode=xml&api_key=aeb4b5888f8206876b3d6be7c77b3fc87308"]
#Date_format:YYYY/MM/DD
pubmed_headers = ["GM Universal Code", "Full Name", "Author Match","Authorship_Position", "Publication_Type", "Mesh_Headings","Title","URL", "Query Used",
          "Description","Details","ShortDetails", "Affiliation","Resource","Type","Identifiers","Db","EntrezUID","Properties", "Author_Count", "Abstract_Text"]

trails_headers = ['GM Universal Code', 'Full Name', 'NCT ID', 'URL', 'Verification Status', 'Query Used', 'Trial Name', "Trial Type",'Trial Phase' , 'Overall Status',
 'Start Date', 'End Date', 'Conditions', 'Interventions', 'Matched Associate','Role', 'Facility', 'Region', 'Other Associates', 'Organizations', 'Lead Sponsor(s)']

def make_archive(source, destination):
    base = os.path.basename(destination)
    name = base.split('.')[0]
    format = base.split('.')[1]
    archive_from = os.path.dirname(source)
    archive_to = os.path.basename(source.strip(os.sep))
    print(source, destination, archive_from, archive_to)
    shutil.make_archive(name, format, archive_from, archive_to)
    shutil.move('%s.%s'%(name,format), destination)
    print(name)
    return "output.zip"

def create_xlsx(data=None, data_list=[], local=False,headers=pubmed_headers,sheet_limit=SHEET_LIMIT, folder_name=''):
    main_file_name = "output.xlsx"
    sheet_number = 1
    folder_path = os.path.join(temp_path, folder_name)
    output_folder_path = os.path.join(folder_path, "output_folder")
    print(output_folder_path)
    workbook = xlsxwriter.Workbook(output_folder_path + os.path.sep + main_file_name)
    worksheet = workbook.add_worksheet(name="Sheet{}".format(sheet_number))
    row = 0
    col = 0
    total_files = [main_file_name]
    for header in headers:
        worksheet.write(row, col, header)
        col += 1

    if not local:
        for col_data in data:
            row += 1
            col = 0
            for header in headers:
                #print("header--> {}:data-->{}:type--->{}".format(header,col_data[header],type(col_data[header])))
                worksheet.write(row, col, col_data[header])
                col += 1

            if sheet_number < MAX_SHEETS_PER_XLS:
                if row >= sheet_limit:
                    row = 0
                    col = 0
                    sheet_number += 1
                    worksheet = workbook.add_worksheet(name="Sheet{}".format(sheet_number))
                    for header in headers:
                        worksheet.write(row, col, header)
                        col += 1
            else:
                if row < sheet_limit:
                    continue
                workbook.close()
                file_name = "{}_part-{}.xlsx".format(os.path.splitext(main_file_name)[0], len(total_files))
                sheet_number = 1
                workbook = xlsxwriter.Workbook(temp_path + os.path.sep + file_name)
                worksheet = workbook.add_worksheet(name="Sheet{}".format(sheet_number))
                row = 0
                col = 0
                for header in headers:
                    worksheet.write(row, col, header)
                    col += 1
                total_files.append(file_name)

    else:
        for data in data_list:
            for col_data in data:
                row += 1
                col = 0
                for header in headers:
                    # print("header--> {}:data-->{}:type--->{}".format(header,col_data[header],type(col_data[header])))
                    worksheet.write(row, col, col_data[header])
                    col += 1

                if sheet_number < MAX_SHEETS_PER_XLS:
                    if row >= sheet_limit:
                        row = 0
                        col = 0
                        sheet_number += 1
                        worksheet = workbook.add_worksheet(name="Sheet{}".format(sheet_number))
                        for header in headers:
                            worksheet.write(row, col, header)
                            col += 1
                else:
                    if row < sheet_limit:
                        continue
                    workbook.close()
                    file_name = "{}_part-{}.xlsx".format(os.path.splitext(main_file_name)[0], len(total_files))
                    sheet_number = 1
                    workbook = xlsxwriter.Workbook(temp_path + os.path.sep + file_name)
                    worksheet = workbook.add_worksheet(name="Sheet{}".format(sheet_number))
                    row = 0
                    col = 0
                    for header in headers:
                        worksheet.write(row, col, header)
                        col += 1
                    total_files.append(file_name)

    workbook.close()
    print(len(total_files))

    if len(total_files) == 1:
        return main_file_name
    else:
        print("going in archie")
        return make_archive('uploads/media/%s/output_folder' %(folder_name), 'uploads/media/%s/zipfolder/output.zip' %(folder_name))

def xml_to_json(xml):
    ''' This API converts xml data to json
        parameters:
        ---------------
        xml (str) : Complete xml data read from website or file

        return:
        ---------------
        json(dict) : returns valid dictionary
    '''
    json_string = json.dumps(xmltodict.parse(xml))
    json_data = json.loads(json_string)
    return json.dumps(json_data)

def get_description(auther_list):
    desc = ""
    i = 0
    if type(auther_list) == dict:
        auther_list = [auther_list]
    for author in auther_list:
        i += 1
        if author.get("CollectiveName",None):
            desc += author["CollectiveName"]
        else:
            if author.get("LastName", "") is None:
                author["LastName"] = ""
            if author.get("ForeName", "") is None:
                author["ForeName"] = ""
            if author.get("Initials", "") is None:
                author["Initials"] = ""
            desc += author.get("LastName","") + " " + author.get("Initials","")
        if len(auther_list) == i:
            desc += "."
        else:
            desc += ", "
    return desc

def get_affiliation_details(auther_name, auther_list):
    if type(auther_list) == dict:
        auther_list = [auther_list]
    name_list = auther_name.replace(",","").lower().split(" ")
    for author in auther_list:
        if author.get("AffiliationInfo"):
            if type(author["AffiliationInfo"]) == dict:
                author["AffiliationInfo"] = [author["AffiliationInfo"]]
                data = ""
                i = 0
                if author.get("LastName", "") is None:
                    author["LastName"] = ""
                if author.get("ForeName", "") is None:
                    author["ForeName"] = ""
                if author.get("LastName", "").lower() in name_list or author.get("ForeName", "").lower() in name_list:
                    for aff_info in author["AffiliationInfo"]:
                        data += aff_info["Affiliation"]
                        i = i+ 1
                        if len(author["AffiliationInfo"]) != i:
                            data += ", "
                        else:
                            data += "."

                    return data
    return ""

def get_journal_issue_details(journal_issue):
    data = "{} {} {};".format(journal_issue["PubDate"].get("Year",""), journal_issue["PubDate"].get("Month",""),journal_issue["PubDate"].get("Day",""))
    if journal_issue.get("Volume"):
        data += " {}({}).".format(journal_issue["Volume"],journal_issue.get("Issue",0))
    return data

def get_elocation_details(elocationid):
    if type(elocationid) == dict:
        elocationid = [elocationid]
    data = ""
    for value in elocationid:
        data += "{}: {}. ".format(value["@EIdType"],value["#text"])
    return data

def get_details(journal, elocationid):
    data = "{}. {} {}".format(journal.get("ISOAbbreviation",""),get_journal_issue_details(journal["JournalIssue"]),get_elocation_details(elocationid))
    return data

def get_short_details(journal):
    data = "{}.".format(journal.get("ISOAbbreviation", ""))
    # data = "{}. {}".format(journal.get("ISOAbbreviation", ""), journal["JournalIssue"]["PubDate"].get("Year",""))
    return data

def get_create_date(pub_dates):
    if type(pub_dates) == dict:
        pub_dates = [pub_dates]
    for date in pub_dates:
        if date["@PubStatus"] in ["pubmed", "medline"]:
            data = "{}/{}/{}".format(date.get("Year",""), date["Month"], date["Day"])
            return data
    return ""

def get_first_author(auther_list):
    if type(auther_list) == dict:
        auther_list = [auther_list]
    data = ""
    for author in auther_list:
        try:
            if author.get("LastName", "") is None:
                author["LastName"] = ""
            if author.get("ForeName", "") is None:
                author["ForeName"] = ""
            if author.get("Initials", "") is None:
                author["Initials"] = ""
            data = author.get("LastName","") + " " + author.get("Initials","")
            return data
        except KeyError:
            if author.get("CollectiveName",None):
                data = author["CollectiveName"]
    return data

def get_full_name(name, auther_list):
    if type(auther_list) == dict:
        auther_list = [auther_list]
    name_list = name.replace(",","").lower().split(" ")
    for author in auther_list:
        if author.get("LastName", "") is None:
            author["LastName"] = ""
        if author.get("ForeName", "") is None:
            author["ForeName"] = ""
        if author.get("LastName", "").lower() in name_list or author.get("ForeName", "").lower() in name_list:
            return author.get("LastName", "") + ", " + author.get("ForeName","")
    return name

def get_author_position(name, first_name, last_name, auther_list):
    if type(auther_list) == dict:
        auther_list = [auther_list]
    name_list = name.replace(",","").lower().split(" ")
    for name_ in name_list:
        if len(name_) < 2:
            name_list.remove(name_)
    if first_name is not None:
        name_list.append(first_name.lower())
    if last_name is not None:
        name_list.append(last_name.lower())
    author_pos_list = []
    matched_authers_list = []
    for author in auther_list:
        if author.get("LastName", "") is None:
            author["LastName"] = ""
        if author.get("ForeName", "") is None:
            author["ForeName"] = ""
        if author.get("LastName", "").lower() in name_list or author.get("ForeName", "").lower() in name_list or author.get("CollectiveName","").lower() in name_list:

            author_pos_list.append("{}".format(auther_list.index(author)+1))
            try:
                matched_authers_list.append("{}, {}".format(author.get("LastName", ""), author.get("ForeName", "")))
            except KeyError:
                if author.get("CollectiveName",None):
                    matched_authers_list.append("{}".format(author["CollectiveName"]))

    return " | ".join(author_pos_list), " | ".join(matched_authers_list)


def get_publication_type(publication_type_list):
    data = []
    if type(publication_type_list) == dict:
        publication_type_list = [publication_type_list]
    for type_ in publication_type_list:
        data.append(type_["#text"])
    return ";".join(data)

def get_mesh_headings(mesh_heading_list):
    data = []
    if type(mesh_heading_list) == dict:
        mesh_heading_list = [mesh_heading_list]
    for heading in mesh_heading_list:
        if heading.get("DescriptorName",""):
            data.append(heading["DescriptorName"].get("#text",""))
    return ";".join(data)


def get_properties(pub_dates,auther_list):
    data = "create date:{} | first author:{}".format(get_create_date(pub_dates), get_first_author(auther_list))
    return data

# def multi_threads(name, uid, firstname, initial, lastname, xlsx_data_list, search_type, from_date, to_date, sheet_len, folder_name, ids_return_data):
#     if name is not None:
#         try:
#             search_data = search_citations(1, name=name, search_type=search_type, initial=initial, lastname=lastname, firstname=firstname, universal_id=uid, from_date=from_date, to_date=to_date, records_per_page=4000, local_searh=True, sheet_len=sheet_len, folder_name=folder_name)                  
#         except Exception as ex:
#             if xlsx_data_list:
#                 print("in xlsx_data_list exception")
#                 return xlsx_data_list
#             else:
#                 print("in except pass")
#                 return xlsx_data_list
#             # raise Exception(str(ex))

#         if search_data is None:
#             print("search data is none")
#             return xlsx_data_list

#         if search_type == "Clinical Trails":
#             xlsx_data_list.append(search_data)
#             ids_return_data = {"ids_info":{},"count":1}
#             return xlsx_data_list

#         if search_data["count"] != 0:
#             ids_return_data["ids_info"].update(search_data["ids_info"])               
#             ids_return_data["count"] = ids_return_data["count"] + len(list(search_data["ids_info"].keys()))
#             xlsx_data_list.append(download_csv(search_data, local=True, folder_name=folder_name))
#             return xlsx_data_list
#     return 'yes'

@app.route('/do_upload/<int:page_num>', methods=['POST'])
def do_upload(page_num):

    if request.method == 'POST':
        result = uuid.uuid4()
        folder_name = result.hex

        status = "In Progress"
        requesttype = "Pubmed and Clinical Trails"
        a = UserQuery(folder_name, username, status, requesttype, page_num)

        file = request.files['upload']
        print(file.filename)
        os.mkdir("uploads/media/%s" % folder_name)
        os.mkdir("uploads/media/%s/input_folder" % folder_name)
        os.mkdir("uploads/media/%s/output_folder" % folder_name)
        os.mkdir("uploads/media/%s/zipfolder" % folder_name)

        file_folder = "uploads/media/%s/input_folder" % folder_name
        file.save(os.path.join(file_folder, file.filename))

        if os.getenv('FLASK_ENV')=="production":
            s3_resource.meta.client.upload_file("uploads/media/%s/input_folder/%s" %(folder_name, file.filename), bucket_name, '%s/input_folder/%s' %(folder_name, file.filename))

        from_date = request.form['from_date'] if request.form['from_date'] else None
        to_date = request.form['to_date'] if request.form['to_date'] else None
        search_type = request.form["search_type"] if request.form['search_type'] else None
        sheet_len = int(request.form["sheet_len"]) if request.form['sheet_len'] else SHEET_LIMIT

        header_ = pubmed_headers
        if search_type == "Clinical Trails":
            header_ = trails_headers
        if from_date and to_date:
            if datetime.strptime(from_date, "%Y-%m-%d") > datetime.strptime(to_date, "%Y-%m-%d"):
                ################################## error chances ##############################
                return '<html><script>alert("from date should be lesser than to date");</script><html>'
                ################################## error chances ##############################

        try:
            xlsx_file_path = os.path.join(file_folder, file.filename)
            xlsx_data = []
            wb_obj = openpyxl.load_workbook(xlsx_file_path)
            sheet_obj = wb_obj.active
            for i in range(1, sheet_obj.max_row+1):
                row_data = {}
                for j in range(1, sheet_obj.max_column+1):
                    row_data[sheet_obj.cell(row = 1, column = j).value] = sheet_obj.cell(row = i+1, column = j).value
                xlsx_data.append(row_data)

            ids_return_data = {"ids_info":{},"count":0}

            xlsx_data_list = []

            # executor = ThreadPoolExecutor(max_workers=10)
            for column_data in xlsx_data:
                print("\n\n{} of {}\n\n".format(xlsx_data.index(column_data)+1, len(xlsx_data)))
                name = column_data["Full_Name"] if column_data.get("Full_Name") else None
                uid = column_data["KOL_ID"] if column_data.get("KOL_ID") else None
                firstname = column_data["First_Name"] if column_data.get("First_Name") else None
                initial = column_data["Middle_Name"] if column_data.get("Middle_Name") else None
                lastname = column_data["Last_Name"] if column_data.get("Last_Name") else None
                # a = executor.submit(multi_threads, name, uid, firstname, initial, lastname, xlsx_data_list, search_type, from_date, to_date, sheet_len, folder_name, ids_return_data)

                # return_value = a.result()
                print("name", name)
                if name is not None:
                    try:
                        search_data = search_citations(1, name=name, search_type=search_type, initial=initial, lastname=lastname, firstname=firstname, universal_id=uid, from_date=from_date, to_date=to_date, records_per_page=4000, local_searh=True, sheet_len=sheet_len, folder_name=folder_name)
                    except Exception as ex:
                        if xlsx_data_list:
                            print("in xlsx_data_list exception")
                            break
                        else:
                            print("in except pass")
                            pass
                        # raise Exception(str(ex))

                    if search_data is None:
                        print("search data is none")
                        continue

                    if search_type == "Clinical Trails":
                        xlsx_data_list.append(search_data)
                        ids_return_data = {"ids_info":{},"count":1}
                        continue

                    if search_data["count"] != 0:
                        ids_return_data["ids_info"].update(search_data["ids_info"])
                        ids_return_data["count"] = ids_return_data["count"] + len(list(search_data["ids_info"].keys()))
                        xlsx_data_list.append(download_csv(search_data, local=True, folder_name=folder_name))

            if ids_return_data["count"] != 0:
                print("going into create_xlsx")
                file_name = create_xlsx(data_list=xlsx_data_list, local=True, headers=header_, sheet_limit=sheet_len, folder_name=folder_name)
                print("file", file_name)

                if file_name == "output.zip":
                    if os.getenv('FLASK_ENV')=="production":
                        s3_resource.meta.client.upload_file("uploads/media/%s/zipfolder/%s" %(folder_name, file_name), bucket_name, '%s/zipfolder/%s' %(folder_name,file_name))
                        if os.path.exists("uploads/media/%s" %folder_name):
                            try:
                                shutil.rmtree("uploads/media/%s" %folder_name, ignore_errors=True)
                                print("deleted")
                            except OSError as e:
                                print ("Error: %s - %s." % (e.filename, e.strerror))
                else:
                    if os.getenv('FLASK_ENV')=="production":
                        s3_resource.meta.client.upload_file("uploads/media/%s/output_folder/%s" %(folder_name, file_name), bucket_name, '%s/output_folder/%s' %(folder_name,file_name))
                        if os.path.exists("uploads/media/%s" %folder_name):
                            try:
                                shutil.rmtree("uploads/media/%s" %folder_name, ignore_errors=True)
                                print("deleted")
                            except OSError as e:
                                print ("Error: %s - %s." % (e.filename, e.strerror))

                status = "Completed"
                userid = UserQuery(folder_name, username, status, requesttype, page_num)
                db_query_user = db.session.query(User)
                query_filter = db_query_user.filter(User.username == username)
                for i in query_filter:
                    tenantid = i.tenantid

                if role == 'Superadmin':
                    all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
                    return render_template("pubmed_clinical_data.html", username=username, all_json_list=all_json_list, role=role)
                elif role == "Admin":
                    all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype, UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
                    return render_template("pubmed_clinical_data.html", username=username, all_json_list=all_json_list, role=role)
                else:
                    json_list = UserRequest.query.filter(UserRequest.userid == userid, UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
                    return render_template("pubmed_clinical_data.html", username=username, json_list=json_list, role=role)

        except Exception as ex:
            print("Exception in upload:{}".format(ex))
            # abort(500, "Exception occurred: {}".format(ex))
            pass

@app.route('/pubmed_clinicaltrail_upload_output', methods=['POST'])
def pubmed_clinicaltrail_upload_output():
    pubmed_data_output = request.form.get("uuidno_output")
    if os.getenv('FLASK_ENV')=="production":
        try:
            s3_resource.Object("glocalmindkoldata", "%s/zipfolder/output.zip" %pubmed_data_output).load()

            file = s3_client.get_object(Bucket='glocalmindkoldata', Key="%s/zipfolder/output.zip" %pubmed_data_output)
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=output.zip"}
            )
        except:
            file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/output_folder/output.xlsx' %pubmed_data_output)
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=output.xlsx"}
            )
    else:
        outputpath = "uploads/media/%s/zipfolder" % pubmed_data_output
        file_name = ""
        for f in os.listdir(outputpath):
            file_name = f
        if file_name == "output.zip":
            path = "uploads/media/%s/zipfolder/%s" % (pubmed_data_output, file_name)
            return send_file(path, as_attachment=True)
        else:
            path = "uploads/media/%s/output_folder/output.xlsx" % (pubmed_data_output)
            return send_file(path, as_attachment=True)


@app.route('/pubmed_clinicaltrail_upload_input', methods=['POST'])
def pubmed_clinicaltrail_upload_input():
    pubmed_data_input = request.form.get("uuidno_input")
    if os.getenv('FLASK_ENV')=="production":
        try:
            my_bucket = s3_resource.Bucket('glocalmindkoldata')
            for object_summary in my_bucket.objects.filter(Prefix="%s/input_folder/" %pubmed_data_input):
                print("input file path: ",object_summary.key)
                file_path = object_summary.key
                file_name = file_path.split('/')[1:][-1]

            file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/input_folder/%s' %(pubmed_data_input,file_name))
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=input.xlsx"}
            )
        except:
            return "Sorry, Invalid Request!! No file uploaded for this request."
    else:
        inputpath = "uploads/media/%s/input_folder" % pubmed_data_input
        file_name = ""
        for f in os.listdir(inputpath):
            file_name = f
        path = "uploads/media/%s/input_folder/%s" % (pubmed_data_input, file_name)
        return send_file(path, as_attachment=True)

def download_csv(query_data=None, local=False, sheet_limit=SHEET_LIMIT, folder_name=''):
    try:
        print("starting into d_csv")
        xlsx_data = []
        if not query_data:
            raise Exception("query_data is madatory field")
        if not query_data.get("count") or not query_data.get("ids_info"):
            raise Exception("Invalid data provided")

        if len(list(query_data["ids_info"].keys())) == 1 :
            ids = list(query_data["ids_info"].keys())[0]
        else:
            ids = ",".join(list(query_data["ids_info"].keys()))

        url = random.choice(PUBMED_DOWNLOAD_CSV)
        print("pubmed download csv url: {}".format(url))
        print("trying to request")
        r = requests.post(url=url, data="id={}".format(ids),headers= {"accept": "application/xml"})
        print("code:", r.status_code)
        if r.status_code == 200:
            json_data = json.loads(xml_to_json(r.text))
            if json_data["PubmedArticleSet"].get("PubmedArticle", None) is None:
                if local:
                    return xlsx_data
                raise Exception("No data found")
            if type(json_data["PubmedArticleSet"]["PubmedArticle"]) == dict:
                json_data["PubmedArticleSet"]["PubmedArticle"] = [json_data["PubmedArticleSet"]["PubmedArticle"]]

            data_count = 1
            for data in json_data["PubmedArticleSet"]["PubmedArticle"]:
                medline_data = data["MedlineCitation"]
                article_data = medline_data["Article"]
                publication_date = data["PubmedData"]
                mesh_heading_data = medline_data.get("MeshHeadingList",{})
                form_data = {}
                print("Name---->{}, data found-->({}/{})".format( query_data["ids_info"][medline_data["PMID"]["#text"]]["name"], data_count,len(list(query_data["ids_info"].keys()))))
                full_name = query_data["ids_info"][medline_data["PMID"]["#text"]]["name"]
                first_name = query_data["ids_info"][medline_data["PMID"]["#text"]]["firstname"]
                last_name = query_data["ids_info"][medline_data["PMID"]["#text"]]["lastname"]

                data_count += 1
                if type(article_data["ArticleTitle"]) == dict:
                    form_data["Title"] = article_data["ArticleTitle"]["#text"]
                else:
                    form_data["Title"] = article_data["ArticleTitle"]
                form_data["URL"] = "https://www.ncbi.nlm.nih.gov/pubmed/" + medline_data["PMID"]["#text"]
                form_data["Description"] = get_description(article_data["AuthorList"]["Author"])
                if article_data.get("ELocationID"):
                    form_data["Details"] = get_details(article_data["Journal"], article_data["ELocationID"])
                else:

                    form_data["Details"] = "{}. {}".format(article_data["Journal"].get("ISOAbbreviation",""),get_journal_issue_details(article_data["Journal"]["JournalIssue"]))

                form_data["GM Universal Code"] = query_data["ids_info"][medline_data["PMID"]["#text"]]["univeral_id"]
                form_data["Affiliation"] = get_affiliation_details(query_data["ids_info"][medline_data["PMID"]["#text"]]["name"], article_data["AuthorList"]["Author"])
                form_data["Query Used"] = query_data["ids_info"][medline_data["PMID"]["#text"]]["query"]
                form_data["ShortDetails"] = get_short_details(article_data["Journal"])
                form_data["Resource"] = "PubMed"
                form_data["Type"] = "citation"
                form_data["Full Name"] =   query_data["ids_info"][medline_data["PMID"]["#text"]]["name"]    #get_full_name(query_data["ids_info"][medline_data["PMID"]["#text"]]["name"], article_data["AuthorList"]["Author"])
                form_data["Identifiers"] = "PMID:" + medline_data["PMID"]["#text"]
                form_data["Db"] = "pubmed"
                if article_data.get("PublicationTypeList"):
                    form_data["Publication_Type"] = get_publication_type(article_data["PublicationTypeList"].get("PublicationType",[]))
                else:
                    form_data["Publication_Type"] = ""

                if mesh_heading_data.get("MeshHeading"):
                    form_data["Mesh_Headings"] = get_mesh_headings(mesh_heading_data.get("MeshHeading",[]))
                else:
                    form_data["Mesh_Headings"] = ""

                form_data["Authorship_Position"],form_data["Author Match"]  = get_author_position(full_name, first_name, last_name, article_data["AuthorList"]["Author"])
                form_data["Author_Count"] = len(article_data["AuthorList"]["Author"])
                form_data["Abstract_Text"] = ""
                if article_data.get("Abstract",None):
                    if article_data["Abstract"].get("AbstractText",""):
                        if type(article_data["Abstract"]["AbstractText"]) == list:
                            for text in article_data["Abstract"]["AbstractText"]:
                                if text:
                                    if type(text) == dict:
                                        form_data["Abstract_Text"] += text.get("#text","")
                                    else:
                                        form_data["Abstract_Text"] += text
                                form_data["Abstract_Text"] += ";"

                        elif type(article_data["Abstract"]["AbstractText"]) == dict:
                            form_data["Abstract_Text"] = article_data["Abstract"]["AbstractText"].get("#text")
                        else:
                            form_data["Abstract_Text"] = article_data["Abstract"]["AbstractText"]

                form_data["EntrezUID"] = medline_data["PMID"]["#text"]
                # form_data["Author Match"] = get_full_name(query_data["ids_info"][medline_data["PMID"]["#text"]]["name"], article_data["AuthorList"]["Author"])
                form_data["Properties"] = get_properties(publication_date["History"]["PubMedPubDate"], article_data["AuthorList"]["Author"])
                xlsx_data.append(form_data)
        else:
            raise Exception("Error occured while comunicating with pubmed, status_code({}),text({})".format(r.status_code,r.text))

        if not xlsx_data:
            raise Exception("Error occured while comunicating with pubmed")

        if local:
            return xlsx_data

        print("going to create xlsx from download csv")
        file_name = create_xlsx(data=xlsx_data, local=False, sheet_limit=sheet_limit, folder_name=folder_name)

        return file_name
    except Exception as ex:
        print("download_csv:Exception occurred: {}".format(ex))
        if local:
            if xlsx_data:
                print("yes xlsx data")
                return xlsx_data
        pass
        # abort(500, "Exception occurred: {}".format(ex))

@app.route("/search_citations/<int:page_num>", methods=['POST'])
def search_citations(page_num, name=None, search_type="Pubmed", initial=None, lastname=None, firstname=None, universal_id=None, from_date=None, to_date=None,  records_per_page="400", local_searh=False, sheet_len=SHEET_LIMIT, folder_name=None):
    try:
        if not local_searh:
            name = request.form['FullName']  if request.form['FullName'] else None
            records_per_page = request.form['records'] if request.form['records'] else "420"
            universal_id = request.form['Uid'] if request.form['Uid'] else str(uuid.uuid4().hex)
            from_date = request.form['from_date']  if request.form['from_date'] else None
            to_date = request.form['to_date']  if request.form['to_date'] else None
            initial = request.form['MiddleName'] if request.form['MiddleName']  else None
            lastname = request.form['LastName']  if request.form['LastName'] else None
            firstname = request.form["FirstName"] if request.form['FirstName'] else None
            search_type = request.form["search_type"] if request.form['search_type'] else None
            sheet_len = int(request.form["sheet_len"]) if request.form['sheet_len'] else SHEET_LIMIT

            result = uuid.uuid4()
            folder_name = result.hex
            print(folder_name)
            os.mkdir("uploads/media/%s" % folder_name)
            os.mkdir("uploads/media/%s/output_folder" % folder_name)
            os.mkdir("uploads/media/%s/zipfolder" % folder_name)

            status = "In Progress"
            requesttype = "Pubmed and Clinical Trails"
            a = UserQuery(folder_name, username, status, requesttype, page_num)

        if search_type == "Clinical Trails":
            file_name = clinical_trails(name, universal_id,from_date,to_date, lastname=lastname, initial=initial, firstname=firstname,local=local_searh,sheet_limit=sheet_len, folder_name=folder_name)

            if local_searh:
                return file_name
            else:
                if file_name is not None:
                    if file_name == "output.zip":
                        if os.getenv('FLASK_ENV')=="production":
                            s3_resource.meta.client.upload_file("uploads/media/%s/zipfolder/%s" %(folder_name, file_name), bucket_name, '%s/zipfolder/%s' %(folder_name,file_name))
                            if os.path.exists("uploads/media/%s" %folder_name):
                                try:
                                    shutil.rmtree("uploads/media/%s" %folder_name, ignore_errors=True)
                                    print("deleted")
                                except OSError as e:
                                    print ("Error: %s - %s." % (e.filename, e.strerror))
                    else:
                        if os.getenv('FLASK_ENV')=="production":
                            s3_resource.meta.client.upload_file("uploads/media/%s/output_folder/%s" %(folder_name, file_name), bucket_name, '%s/output_folder/%s' %(folder_name,file_name))
                            if os.path.exists("uploads/media/%s" %folder_name):
                                try:
                                    shutil.rmtree("uploads/media/%s" %folder_name, ignore_errors=True)
                                    print("deleted")
                                except OSError as e:
                                    print ("Error: %s - %s." % (e.filename, e.strerror))

                    status = "Completed"
                    userid = UserQuery(folder_name, username, status, requesttype, page_num)
                    db_query_user = db.session.query(User)
                    query_filter = db_query_user.filter(User.username == username)
                    for i in query_filter:
                        tenantid = i.tenantid

                    if role == 'Superadmin':
                        all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
                        return render_template("pubmed_clinical_data.html", username=username, all_json_list=all_json_list, role=role)
                    elif role == "Admin":
                        all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype, UserRequest.tenantid == tenantid).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
                        return render_template("pubmed_clinical_data.html", username=username, all_json_list=all_json_list, role=role)
                    else:
                        json_list = UserRequest.query.filter(UserRequest.userid == userid, UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
                        return render_template("pubmed_clinical_data.html", username=username, json_list=json_list, role=role)

                return "No Data found"

    ##############################################  search type == Pubmed ################################################33333

        url = None
        if not name:
            raise Exception("Name is mandatory field")

        search_name = "" + name + "[Full Author Name]"
        if search_name:

            if firstname and initial and lastname:
                search_name += '+OR+"{}{} {}"[Full Author Name]'.format(firstname[0], initial[0],lastname)
                search_name += '+OR+"{}, {}{}"[Full Author Name]'.format(lastname, firstname[0], initial[0])
                search_name += '+OR+"{} {}{}"[Author]'.format(lastname, firstname[0], initial[0])
                search_name += '+OR+"{}, {} {}"[Full Author Name]'.format(lastname, firstname, initial)
                search_name += '+OR+"{}, {} {}"[Full Author Name]'.format(lastname, firstname, initial[0])
                search_name += '+OR+"{} {} {}"[Author]'.format(initial, lastname, firstname)

            if lastname and firstname:
                search_name += '+OR+"{}, {}"[Full Author Name]'.format(lastname, firstname)
                search_name += '+OR+"{} {}"[Author]'.format(firstname, lastname)
                search_name += '+OR+"{}, {}"[Full Author Name]'.format(lastname, firstname[0])
                search_name += '+OR+"{} {}"[Author]'.format(lastname, firstname[0])


            url = random.choice(PUBMED_SEARCH_URL).format(search_name)

        if from_date and to_date:
            if datetime.strptime(from_date, "%Y-%m-%d") > datetime.strptime(to_date, "%Y-%m-%d"):
                return '<html><script>alert("from date should be lesser than to date");</script><html>'

            url = url + PUBMED_DATE_QUERY.format(datetime.strptime(from_date, '%Y-%m-%d').strftime('%Y/%m/%d'), datetime.strptime(to_date, '%Y-%m-%d').strftime('%Y/%m/%d'))

        query_url = url.split("&term=")[-1]

        if not url:
            raise Exception("Unable to build search url")
        url += "&retmax={}".format(records_per_page)

        print("pubmed search url: {}".format(url))
        # try:
        r = requests.post(url=url,headers= {"Content-Type": "application/xml","accept": "application/xml"})
        # print("fetching requests")
        if r.status_code == 200:
            json_data = json.loads(xml_to_json(r.text))
        else:
            raise Exception("Error occured while comunicating with pubmed{}".format(r.status_code))

        if int(json_data["eSearchResult"]["Count"]) <= 0:
            if local_searh:
                return {"ids_info":{},"count":0 }
            else:
                return "No records found for this"

        id_list = json_data["eSearchResult"]["IdList"].get("Id",None)

        if type(id_list) in [str, int]:
            id_list = [id_list]

        return_data = {"ids_info":{}}
        for _id in id_list:
            return_data["ids_info"][_id] = {"name": name,"lastname":lastname, "firstname":firstname,"query":query_url,"univeral_id":universal_id}

        return_data["count"] = len(list(return_data["ids_info"].keys()))

        print("Total records found ------------->{}".format(return_data["count"]))
        if return_data["count"] != 0:

            if not local_searh:
                file_name = download_csv(query_data=return_data, local=local_searh,sheet_limit=sheet_len, folder_name=folder_name)

                if file_name == "output.zip":
                    if os.getenv('FLASK_ENV')=="production":
                        s3_resource.meta.client.upload_file("uploads/media/%s/zipfolder/%s" %(folder_name, file_name), bucket_name, '%s/zipfolder/%s' %(folder_name,file_name))
                        if os.path.exists("uploads/media/%s" %folder_name):
                            try:
                                shutil.rmtree("uploads/media/%s" %folder_name, ignore_errors=True)
                                print("deleted")
                            except OSError as e:
                                print ("Error: %s - %s." % (e.filename, e.strerror))
                else:
                    if os.getenv('FLASK_ENV')=="production":
                        s3_resource.meta.client.upload_file("uploads/media/%s/output_folder/%s" %(folder_name, file_name), bucket_name, '%s/output_folder/%s' %(folder_name,file_name))
                        if os.path.exists("uploads/media/%s" %folder_name):
                            try:
                                shutil.rmtree("uploads/media/%s" %folder_name, ignore_errors=True)
                                print("deleted")
                            except OSError as e:
                                print ("Error: %s - %s." % (e.filename, e.strerror))

                status = "Completed"
                userid = UserQuery(folder_name, username, status, requesttype, page_num)

                if role == "Admin" or role == 'Superadmin':
                    all_json_list = UserRequest.query.join(User, UserRequest.userid==User.id).add_columns(UserRequest.tenantid, UserRequest.userid, UserRequest.requesttype, UserRequest.status, UserRequest.uuidno, UserRequest.requestdate, User.username).filter(UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
                    return render_template("pubmed_clinical_data.html", username=username, all_json_list=all_json_list, role=role)
                else:
                    json_list = UserRequest.query.filter(UserRequest.userid == userid, UserRequest.requesttype == requesttype).order_by(UserRequest.requestdate.desc()).paginate(per_page=5, page=page_num, error_out=True)
                    return render_template("pubmed_clinical_data.html", username=username, json_list=json_list, role=role)

            else:
                return return_data

    except Exception as ex:
        if local_searh:
            print("in except of search citation")
            pass
        print("search_citations:Exception occurred: {}".format(ex))
        # abort(500, "Exception occurred: {}".format(ex))

@app.route('/pubmed_clinicaltrail_search_output', methods=['POST'])
def pubmed_clinicaltrail_search_output():
    pubmed_data_output = request.form.get("uuidno_output")
    if os.getenv('FLASK_ENV')=="production":
        try:
            s3_resource.Object("glocalmindkoldata", "%s/zipfolder/output.zip" %pubmed_data_output).load()
            file = s3_client.get_object(Bucket='glocalmindkoldata', Key="%s/zipfolder/output.zip" %pubmed_data_output)
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=output.zip"}
            )
        except:
            file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/output_folder/output.xlsx' %pubmed_data_output)
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=output.xlsx"}
            )
    else:
        outputpath = "uploads/media/%s/zipfolder" % pubmed_data_output
        file_name = ""
        for f in os.listdir(outputpath):
            file_name = f
        if file_name == "output.zip":
            path = "uploads/media/%s/zipfolder/%s" % (pubmed_data_output, file_name)
            return send_file(path, as_attachment=True)
        else:
            path = "uploads/media/%s/output_folder/output.xlsx" % (pubmed_data_output)
            return send_file(path, as_attachment=True)

def get_interventions(interventions_list):
    data = ""
    i = 0
    if type(interventions_list) == dict:
        interventions_list = [interventions_list]
    for interventions in interventions_list:
        data += interventions["intervention_name"]
        i += 1
        if i != len(interventions_list):
            data += " | "

    return data

def get_role(overall_list):
    if type(overall_list) == dict:
        overall_list = [overall_list]
    data = ""
    i = 0
    for v in overall_list:
        data += v.get("role","")
        i += 1
        if i != len(overall_list):
            data += " | "
    return data

def get_facilities(facilities, name, last_name=None):
    if type(facilities) == dict:
        facilities = [facilities]
    search_name = name
    if last_name:
        search_name = [last_name]
    else:
        search_name = name.split(" ")


    for facility in facilities:
        if not facility["facility"].get("name"):
            continue
        for name_ in search_name:
            if name_ in facility["facility"]["name"]:
                return facility["facility"]["name"], "{}_{}_{}".format(facility["facility"]["address"].get("country"),facility["facility"]["address"].get("city"), facility["facility"]["address"].get("zip"))

    return "", ""

def get_other_associates(facilities):
    if type(facilities) == dict:
        facilities = [facilities]
    data = ""
    for facility in facilities:
        if not facility["facility"].get("name"):
            continue
        data += facility["facility"]["name"] + "|"
    return data

def get_sponsers(sponsers_list):
    if type(sponsers_list) == dict:
        sponsers_list = [sponsers_list]
    data = ""
    for sponser in sponsers_list:
        data += sponser["lead_sponsor"]["agency"]
    return data

def get_start_date(date_):
    if type(date_) == dict:
        return date_["#text"]
    else:
        return date_

def get_matched_associate(overall_official_list, name, lastname):
    if type(overall_official_list) == dict:
        overall_official_list = [overall_official_list]
    search_name_list = []
    if lastname:
        lastname = lastname.lower()
        lastname = lastname.replace(","," ")
        lastname_list = lastname.split(" ")
        try:
            lastname_list.remove(" ")
        except:
            pass
        search_name_list = lastname_list
    else:
        name = name.lower()
        name = name.replace(","," ")
        name_list = name.split(" ")
        try:
            name_list.remove(" ")
        except:
            pass
        search_name_list = name_list

    for official in overall_official_list:
        if official.get("last_name"):
            official_last_name = official["last_name"].lower()
            official_last_name = official_last_name.replace(","," ")
            official_last_name_list = official_last_name.split(" ")
            for name in official_last_name_list:
                if name in search_name_list:
                    return official["last_name"]
    return ""


def clinical_trails(name, _uuid,from_date,to_date, lastname=None, initial=None, firstname=None, local=False,sheet_limit=SHEET_LIMIT, folder_name=''):
    print("name:{},lastname:{}".format(name,lastname))
    print(from_date)
    print(to_date)
    try:

        t = Trials(from_date,to_date)
        zip_folder_path = os.path.join(temp_path,str(uuid.uuid4().hex))
        if not os.path.exists(zip_folder_path):
            os.makedirs(zip_folder_path)
        file_path = os.path.join(zip_folder_path,str(uuid.uuid4().hex)+ ".zip")
        zip_file_ = open(file_path,"wb")
        if lastname:
            zip_data = t.download(search_term=lastname)
        else:
            zip_data = t.download(search_term=name)

        zip_file_.write(zip_data)
        zip_file_.close()
        import zipfile
        try:
            zip_ref = zipfile.ZipFile(file_path, 'r')
            zip_ref.extractall(zip_folder_path)
            zip_ref.close()
        except Exception as ex:
            os.remove(file_path)
            os.rmdir(zip_folder_path)
            if local:
                return []
            return None

        os.remove(file_path)
        files = os.listdir(zip_folder_path)


        print("Search results--->{}".format(len(files)))
        clinical_trails_data = []
        for file_ in files:

            print(file_)

            xml_file = os.path.join(zip_folder_path, file_)
            xml_file_obj = open(xml_file,"r", encoding="utf8")
            xml_data = xml_file_obj.read()
            json_data = json.loads(xml_to_json(xml_data))
            clinical_trails_data.append(json_data)
            xml_file_obj.close()
            os.remove(xml_file)
        os.rmdir(zip_folder_path)

        xlsx_data = []
        for data in clinical_trails_data:
            clinical_study = data["clinical_study"]
            form_data = {}
            form_data["GM Universal Code"] = _uuid
            form_data["Full Name"] = name
            form_data["NCT ID"] = clinical_study["id_info"]["nct_id"]
            form_data["URL"] = clinical_study["required_header"]["url"]
            form_data["Verification Status"] = "Probable"
            if lastname:
                form_data["Query Used"] = lastname
            else:
                form_data["Query Used"] = name
            form_data["Trial Name"] = clinical_study["brief_title"]
            form_data["Trial Phase"] = clinical_study.get("phase","N/A")

            form_data["Overall Status"] = clinical_study["overall_status"]
            form_data["Start Date"] = get_start_date(clinical_study["start_date"]) if clinical_study.get("start_date") else ""

            if clinical_study.get("completion_date"):

                form_data["End Date"] = clinical_study["completion_date"]["#text"] if type(clinical_study["completion_date"]) == dict else clinical_study["completion_date"]
            elif clinical_study.get("primary_completion_date"):
                form_data["End Date"] = clinical_study["primary_completion_date"]["#text"] if type(clinical_study["primary_completion_date"]) == dict else clinical_study["primary_completion_date"]
            else:
                form_data["End Date"] = "N/A"

            if clinical_study.get("condition"):
                form_data["Conditions"] = " | ".join(clinical_study["condition"]) if type(clinical_study["condition"]) == list else clinical_study["condition"]
            else:
                form_data["Conditions"] = ""

            if clinical_study.get("intervention"):
                form_data["Interventions"] = get_interventions(clinical_study["intervention"])
            else:
                form_data["Interventions"] = ""
            if clinical_study.get("overall_official"):
                form_data["Role"] =  get_role(clinical_study["overall_official"])
            else:
                form_data["Role"] = ""
            form_data["Associate Type"] = "N/A"
            if clinical_study.get("location"):
                form_data['Facility'],form_data['Region'] = get_facilities(clinical_study["location"], name,lastname)
                form_data["Other Associates"] = get_other_associates(clinical_study["location"])
            else:
                form_data['Facility'] = ""
                form_data["Other Associates"] = ""
                form_data['Region'] = ""
            form_data["Trial Type"] = clinical_study.get("study_type","")
            form_data["Matched Associate"] = get_matched_associate(clinical_study.get("overall_official",[]), name, lastname)
            form_data["Organizations"] = clinical_study["source"]
            form_data["Lead Sponsor(s)"] = get_sponsers(clinical_study["sponsors"])
            xlsx_data.append(form_data)

        if not xlsx_data:
            raise Exception("Error occured while comunicating with clinical Trials")
        if local:
            return xlsx_data

        file_name = create_xlsx(data=xlsx_data, local=False, headers=trails_headers,sheet_limit=sheet_limit, folder_name=folder_name)

        return file_name

    except Exception as ex:
        print("clinical_trails exception occurred:{}".format(ex))
        if local:
            # if xlsx_data:
            #     return xlsx_data
            # else:
            pass
        raise Exception(str(ex))

@app.route("/clear_tmp", methods=['POST'])
def clear_tmp():
    files = os.listdir(temp_path)
    for file_ in files:
        os.remove(file_)

@app.route("/css/<css_file>")
def serve_css(css_file):
    path = os.path.join(os.path.dirname(os.path.abspath(__file__))+os.path.sep+"css", css_file)
    return send_file(path, as_attachment=True)

############################################################################################################################
@app.route('/download_input', methods=['POST'])
def download_input():
    got_response = request.form.get("uuidno_input")
    got_response = got_response[1:-1]
    str_uuid = got_response.split(',')[0]
    uuid = str_uuid[1:-1]
    type_dwnld = got_response.split(',')[1]
    type_download = type_dwnld[2:-1]
    print(type_download, uuid)

    if type_download == "Raw":
        if os.getenv('FLASK_ENV')=="production":
            file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/input_folder/raw_data.xlsx' %uuid)
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=raw_data.xlsx"}
            )
        else:
            path = "uploads/media/%s/input_folder/raw_data.xlsx" % uuid
            return send_file(path, as_attachment=True)
    elif type_download == "Profiling":
        if os.getenv('FLASK_ENV')=="production":
            file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/input_folder/profiling_data.xlsx' %uuid)
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=profiling_data.xlsx"}
            )
        else:
            path = "uploads/media/%s/input_folder/profiling_data.xlsx" % uuid
            return send_file(path, as_attachment=True)
    elif type_download == "Twitter":
        inputpath = "uploads/media/%s/input_folder" % uuid
        if os.getenv('FLASK_ENV')=="production":
            my_bucket = s3_resource.Bucket('glocalmindkoldata')
            for object_summary in my_bucket.objects.filter(Prefix="%s/input_folder/" %uuid):
                print("input file path: ",object_summary.key)
                file_path = object_summary.key
                file_name = file_path.split('/')[1:][-1]
            file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/input_folder/%s' %(uuid,file_name))
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=input.xlsx"}
            )
        else:
            file_name = ""
            for f in os.listdir(inputpath):
                file_name = f
            path = "uploads/media/%s/input_folder/%s" % (uuid, file_name)
            return send_file(path, as_attachment=True)
    elif type_download == "Crawler":
        inputpath = "uploads/media/%s/input_folder" % uuid
        if os.getenv('FLASK_ENV')=="production":
            my_bucket = s3_resource.Bucket('glocalmindkoldata')
            for object_summary in my_bucket.objects.filter(Prefix="%s/input_folder/" %uuid):
                print("input file path: ",object_summary.key)
                file_path = object_summary.key
                file_name = file_path.split('/')[1:][-1]
            file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/input_folder/%s' %(uuid,file_name))
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=input.xlsx"}
            )
        else:
            file_name = ""
            for f in os.listdir(inputpath):
                file_name = f
            path = "uploads/media/%s/input_folder/%s" % (uuid, file_name)
            return send_file(path, as_attachment=True)

    elif type_download == "Pubmed and Clinical Trails":
        inputpath = "uploads/media/%s/input_folder" % uuid
        if os.getenv('FLASK_ENV')=="production":
            try:
                my_bucket = s3_resource.Bucket('glocalmindkoldata')
                for object_summary in my_bucket.objects.filter(Prefix="%s/input_folder/" %uuid):
                    print("input file path: ",object_summary.key)
                    file_path = object_summary.key
                    file_name = file_path.split('/')[1:][-1]
                file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/input_folder/%s' %(uuid,file_name))
                return Response(
                    file['Body'].read(),
                    mimetype='text/plain',
                    headers={"Content-Disposition": "attachment;filename=input.xlsx"}
                )
            except:
                return "Sorry, Invalid Request!! No file uploaded for this request."
        else:
            file_name = ""
            for f in os.listdir(inputpath):
                file_name = f
            path = "uploads/media/%s/input_folder/%s" % (uuid, file_name)
            return send_file(path, as_attachment=True)


@app.route('/download_output', methods=['POST'])
def download_output():
    got_response = request.form.get("uuidno_output")
    got_response = got_response[1:-1]
    str_uuid = got_response.split(',')[0]
    uuid = str_uuid[1:-1]
    type_dwnld = got_response.split(',')[1]
    type_download = type_dwnld[2:-1]
    print(type_download, uuid)
    if type_download == "Raw":
        if os.getenv('FLASK_ENV')=="production":
            file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/output_folder/rawdata_output_rank.xlsx' %uuid)
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=rawdata_output_rank.xlsx"}
            )
        else:
            path = "uploads/media/%s/output_folder/rawdata_output_rank.xlsx" %uuid
            return send_file(path, as_attachment=True)
    elif type_download == "Profiling":
        if os.getenv('FLASK_ENV')=="production":
            file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/output_folder/profiling_output_rank.xlsx' %uuid)
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=profiling_output_rank.xlsx"}
            )
        else:
            path = "uploads/media/%s/output_folder/profiling_output_rank.xlsx" %uuid
            return send_file(path, as_attachment=True)
    elif type_download == "Twitter":
        if os.getenv('FLASK_ENV')=="production":
            file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/zipfolder/tweet.zip' %uuid)
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=tweet.zip"}
            )
        else:
            path = "uploads/media/%s/zipfolder/tweet.zip" %uuid
            return send_file(path, as_attachment=True)
    elif type_download == "Crawler":
        if os.getenv('FLASK_ENV')=="production":
            file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/zipfolder/output.zip' %uuid)
            return Response(
                file['Body'].read(),
                mimetype='text/plain',
                headers={"Content-Disposition": "attachment;filename=output.zip"}
            )
        else:
            path = "uploads/media/%s/zipfolder/output.zip" % uuid
            return send_file(path, as_attachment=True)

    elif type_download == "Pubmed and Clinical Trails":
        if os.getenv('FLASK_ENV')=="production":
            try:
                s3_resource.Object("glocalmindkoldata", "%s/zipfolder/output.zip" %uuid).load()
                file = s3_client.get_object(Bucket='glocalmindkoldata', Key="%s/zipfolder/output.zip" % uuid)
                return Response(
                    file['Body'].read(),
                    mimetype='text/plain',
                    headers={"Content-Disposition": "attachment;filename=output.zip"}
                )
            except:
                file = s3_client.get_object(Bucket='glocalmindkoldata', Key='%s/output_folder/output.xlsx' % uuid)
                return Response(
                    file['Body'].read(),
                    mimetype='text/plain',
                    headers={"Content-Disposition": "attachment;filename=output.xlsx"}
                )
        else:
            outputpath = "uploads/media/%s/zipfolder" % uuid
            file_name = ""
            for f in os.listdir(outputpath):
                file_name = f
            if file_name == "output.zip":
                path = "uploads/media/%s/zipfolder/%s" % (uuid, file_name)
                return send_file(path, as_attachment=True)
            else:
                path = "uploads/media/%s/output_folder/output.xlsx" % (uuid)
                return send_file(path, as_attachment=True)
#####################################################################################################################################
@app.route('/get_user', methods=['POST'])
def get_user():
    global user_username
    global user_emailid
    global user_contact
    global local_user_search
    try:
        got_response = request.form.get("usersdetail")
        got_response = got_response[1:-1]
        user_username = got_response.split(',')[0][1:-1]
        user_emailid = got_response.split(',')[1][2:-1]
        user_password = got_response.split(',')[1:][1][2:-1]
        user_contact = int(got_response.split(',')[1:][2][10:-2])
        user_country = got_response.split(',')[1:][3][2:-1]
        user_address = ''
        user_address = user_address.join(got_response.split(',')[1:][4:])
        user_address = user_address[2:-1]
        local_user_search = "Not None"
        return render_template("add_user.html", username=username, role=role, user_username=user_username, user_emailid=user_emailid, user_password=user_password, user_contact=user_contact, user_address=user_address, user_country=user_country)

    except:
        got_response = request.form.get("usersalldetail")
        got_response = got_response[1:-1]
        user_clientname = got_response.split(',')[0][1:-1]
        user_username = got_response.split(',')[1][2:-1]
        user_emailid = got_response.split(',')[1:][1][2:-1]
        user_password = got_response.split(',')[1:][2][2:-1]
        user_contact = int(got_response.split(',')[1:][3][10:-2])
        user_country = got_response.split(',')[1:][4][2:-1]
        user_address = ''
        user_address = user_address.join(got_response.split(',')[1:][5:])
        user_address = user_address[2:-1]

        local_user_search = "Not None"
        return render_template("add_user.html", username=username, role=role, user_clientname=user_clientname, user_username=user_username, user_emailid=user_emailid, user_password=user_password, user_contact=user_contact, user_address=user_address, user_country=user_country)

@app.route('/add_user/<int:page_num>', methods=['POST'])
def add_user(page_num):
    try:
        #global local_user_search
        #if local_user_search != None:
        user=1
        if user==1:
            if role == "Superadmin":
                organization_name = request.form['clientname'].replace(" ", "")
                db_query_tenant = db.session.query(Tenant)
                query_tenant_filter = db_query_tenant.filter(Tenant.organization_name == organization_name)
                for a in query_tenant_filter:
                    tenantid = a.tenantid

                name = request.form['name'].replace(" ", "")
                password = request.form['password'].replace(" ", "")
                user_role = request.form['user_role']
                active = request.form['active']

                if active == "Active":
                    active = True
                else:
                    active = False

                format = "%Y-%m-%d %H:%M:%S"
                now_utc = datetime.now(timezone('UTC'))
                now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
                time_updated = now_asia.strftime(format)

                emailid = request.form['emailid'].replace(" ", "")

                contact = int(request.form['contact'].replace(" ", ""))
                address = request.form['address'].replace(" ", "")
                country = request.form['country'].replace(" ", "")

                db_query_user = db.session.query(User)
                query_user_filter = db_query_user.filter(User.emailid == emailid)
                if query_user_filter.count()!=0 and emailid != user_emailid:
                    return "email id already in database"
                query_user_filter = db_query_user.filter(User.contact == contact)
                if query_user_filter.count()!=0 and contact != user_contact:
                    return "contact already in database"
                query_user_filter = db_query_user.filter(User.username == name)
                if query_user_filter.count()!=0 and name != user_username:
                    return "name already taken"


               ## if request.method == 'POST':
                    # f = request.files['myphoto']
                    #
                    # if os.path.exists("static/img/profile_photo/%s" %name):
                    #     try:
                    #         shutil.rmtree("static/img/profile_photo/%s" %name, ignore_errors=True)
                    #     except OSError as e:
                    #         print ("Error: %s - %s." % (e.filename, e.strerror))
                    # os.mkdir("static/img/profile_photo/%s" %name)
                    # file_folder =  "static/img/profile_photo/%s" %name
                    #
                    # try:
                    #     f.save(os.path.join(file_folder, f.filename))
                    # except:
                    #     print("no photo provided")

                query_user_filter = db_query_user.filter(User.username == user_username).update({'tenantid': tenantid, 'username':name, 'password':password, 'active':active, 'time_updated':time_updated, 'emailid':emailid, 'contact':contact, 'address':address, 'country':country},synchronize_session='fetch')
                db.session.commit()
                print("updated to database")

                local_user_search = None

                all_users = User.query.join(Tenant, User.tenantid==Tenant.tenantid).add_columns(User.username, User.password, User.role, User.active, User.emailid, User.contact, User.address, User.country, Tenant.organization_name).order_by(User.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
                return render_template("user_management.html", username=username, all_users=all_users, role=role)

            else:
                db_query_user = db.session.query(User)
                query_user_filter = db_query_user.filter(User.username == username)
                for i in query_user_filter:
                    tenantid = i.tenantid
                name = request.form['name'].replace(" ", "")
                password = request.form['password'].replace(" ", "")
                user_role = request.form['user_role']
                active = request.form['active']

                if active == "Active":
                    active = True
                else:
                    active = False

                format = "%Y-%m-%d %H:%M:%S"
                now_utc = datetime.now(timezone('UTC'))
                now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
                time_created = now_asia.strftime(format)
                now_utc = datetime.now(timezone('UTC'))
                now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
                time_updated = now_asia.strftime(format)
                emailid = request.form['emailid'].replace(" ", "")
                contact = int(request.form['contact'].replace(" ", ""))
                address = request.form['address'].replace(" ", "")
                country = request.form['country'].replace(" ", "")

                query_user_filter = db_query_user.filter(User.emailid == emailid)
                if query_user_filter.count()!=0 and emailid != user_emailid:
                    return "email id already in database"
                query_user_filter = db_query_user.filter(User.contact == contact)
                if query_user_filter.count()!=0 and contact != user_contact:
                    return "contact already in database, try using different"
                query_user_filter = db_query_user.filter(User.username == name)
                if query_user_filter.count()!=0 and name != user_username:
                    return "name already taken"

                #if request.method == 'POST':
                    # f = request.files['myphoto']
                    #
                    # if os.path.exists("static/img/profile_photo/%s" %name):
                    #     try:
                    #         shutil.rmtree("static/img/profile_photo/%s" %name, ignore_errors=True)
                    #     except OSError as e:
                    #         print ("Error: %s - %s." % (e.filename, e.strerror))
                    # os.mkdir("static/img/profile_photo/%s" %name)
                    # file_folder =  "static/img/profile_photo/%s" %name
                    # try:
                    #     f.save(os.path.join(file_folder, f.filename))
                    # except:
                    #     print("no photo provided")

                query_user_filter = db_query_user.filter(User.username == user_username).update({'tenantid': tenantid, 'username':name, 'password':password, 'active':active, 'time_updated':time_updated, 'emailid':emailid, 'contact':contact, 'address':address, 'country':country},synchronize_session='fetch')
                db.session.commit()
                print("updated to database")

                local_user_search = None

                users = User.query.filter(User.tenantid == tenantid).order_by(User.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
                return render_template("user_management.html", username=username, users=users, role=role)
        else:
            if role == "Superadmin":
                organization_name = request.form['clientname'].replace(" ", "")
                db_query_tenant = db.session.query(Tenant)
                query_tenant_filter = db_query_tenant.filter(Tenant.organization_name == organization_name)
                for a in query_tenant_filter:
                    tenantid = a.tenantid

                name = request.form['name'].replace(" ", "")
                password = request.form['password'].replace(" ", "")
                user_role = request.form['user_role']
                active = request.form['active']

                if active == "Active":
                    active = True
                else:
                    active = False

                format = "%Y-%m-%d %H:%M:%S"
                now_utc = datetime.now(timezone('UTC'))
                now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
                time_created = now_asia.strftime(format)
                now_utc = datetime.now(timezone('UTC'))
                now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
                time_updated = now_asia.strftime(format)
                emailid = request.form['emailid'].replace(" ", "")
                contact = int(request.form['contact'].replace(" ", ""))
                address = request.form['address'].replace(" ", "")
                country = request.form['country'].replace(" ", "")

                db_query_user = db.session.query(User)
                query_user_filter = db_query_user.filter(User.emailid == emailid)
                if query_user_filter.count()!=0:
                    return "email id already in database"
                query_user_filter = db_query_user.filter(User.contact == contact)
                if query_user_filter.count()!=0:
                    return "contact already in database, try using different"
                query_user_filter = db_query_user.filter(User.username == name)
                if query_user_filter.count()!=0:
                    return "name already taken"

               # if request.method == 'POST':
                    # f = request.files['myphoto']
                    #
                    # if os.path.exists("static/img/profile_photo/%s" %name):
                    #     try:
                    #         shutil.rmtree("static/img/profile_photo/%s" %name, ignore_errors=True)
                    #     except OSError as e:
                    #         print ("Error: %s - %s." % (e.filename, e.strerror))
                    # os.mkdir("static/img/profile_photo/%s" %name)
                    # file_folder =  "static/img/profile_photo/%s" %name
                    #
                    # try:
                    #     f.save(os.path.join(file_folder, f.filename))
                    # except:
                    #     print("no photo provided")

                data_user = User(tenantid, name, password, user_role, active, time_created, time_updated, emailid, contact, address, country)
                db.session.add(data_user)
                db.session.commit()

                all_users = User.query.join(Tenant, User.tenantid==Tenant.tenantid).add_columns(User.username, User.password, User.role, User.active, User.emailid, User.contact, User.address, User.country, Tenant.organization_name).order_by(User.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
                return render_template("user_management.html", username=username, all_users=all_users, role=role)

            else:
                db_query_user = db.session.query(User)
                query_user_filter = db_query_user.filter(User.username == username)
                for i in query_user_filter:
                    tenantid = i.tenantid
                name = request.form['name'].replace(" ", "")
                password = request.form['password'].replace(" ", "")
                user_role = request.form['user_role']
                active = request.form['active']

                if active == "Active":
                    active = True
                else:
                    active = False

                format = "%Y-%m-%d %H:%M:%S"
                now_utc = datetime.now(timezone('UTC'))
                now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
                time_created = now_asia.strftime(format)
                now_utc = datetime.now(timezone('UTC'))
                now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
                time_updated = now_asia.strftime(format)
                emailid = request.form['emailid'].replace(" ", "")
                contact = int(request.form['contact'].replace(" ", ""))
                address = request.form['address'].replace(" ", "")
                country = request.form['country'].replace(" ", "")

                query_user_filter = db_query_user.filter(User.emailid == emailid)
                if query_user_filter.count()!=0:
                    return "email id already in database"
                query_user_filter = db_query_user.filter(User.contact == contact)
                if query_user_filter.count()!=0:
                    return "contact already in database, try using different"
                query_user_filter = db_query_user.filter(User.username == name)
                if query_user_filter.count()!=0:
                    return "name already taken"

                # if request.method == 'POST':
                #     f = request.files['myphoto']
                #
                #     if os.path.exists("static/img/profile_photo/%s" %name):
                #         try:
                #             shutil.rmtree("static/img/profile_photo/%s" %name, ignore_errors=True)
                #         except OSError as e:
                #             print ("Error: %s - %s." % (e.filename, e.strerror))
                #     os.mkdir("static/img/profile_photo/%s" %name)
                #     file_folder =  "static/img/profile_photo/%s" %name
                #     try:
                #         f.save(os.path.join(file_folder, f.filename))
                #     except:
                #         print("no photo provided")

                data_user = User(tenantid, name, password, user_role, active, time_created, time_updated, emailid, contact, address, country)
                db.session.add(data_user)
                db.session.commit()

                users = User.query.filter(User.tenantid == tenantid).order_by(User.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
                return render_template("user_management.html", username=username, users=users, role=role)
    except:
        if role == "Superadmin":
            organization_name = request.form['clientname'].replace(" ", "")
            db_query_tenant = db.session.query(Tenant)
            query_tenant_filter = db_query_tenant.filter(Tenant.organization_name == organization_name)
            for a in query_tenant_filter:
                tenantid = a.tenantid

            name = request.form['name'].replace(" ", "")
            password = request.form['password'].replace(" ", "")
            user_role = request.form['user_role']
            active = request.form['active']

            if active == "Active":
                active = True
            else:
                active = False

            format = "%Y-%m-%d %H:%M:%S"
            now_utc = datetime.now(timezone('UTC'))
            now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
            time_created = now_asia.strftime(format)
            now_utc = datetime.now(timezone('UTC'))
            now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
            time_updated = now_asia.strftime(format)
            emailid = request.form['emailid'].replace(" ", "")
            contact = int(request.form['contact'].replace(" ", ""))
            address = request.form['address'].replace(" ", "")
            country = request.form['country'].replace(" ", "")

            db_query_user = db.session.query(User)
            query_user_filter = db_query_user.filter(User.emailid == emailid)
            if query_user_filter.count()!=0:
                return "email id already in database"
            query_user_filter = db_query_user.filter(User.contact == contact)
            if query_user_filter.count()!=0:
                return "contact already in database, try using different"
            query_user_filter = db_query_user.filter(User.username == name)
            if query_user_filter.count()!=0:
                return "name already taken"
            #
            # if request.method == 'POST':
            #     f = request.files['myphoto']
            #
            #     if os.path.exists("static/img/profile_photo/%s" %name):
            #         try:
            #             shutil.rmtree("static/img/profile_photo/%s" %name, ignore_errors=True)
            #         except OSError as e:
            #             print ("Error: %s - %s." % (e.filename, e.strerror))
            #     os.mkdir("static/img/profile_photo/%s" %name)
            #     file_folder =  "static/img/profile_photo/%s" %name
            #
            #     try:
            #         f.save(os.path.join(file_folder, f.filename))
            #     except:
            #         print("no photo provided")

            data_user = User(tenantid, name, password, user_role, active, time_created, time_updated, emailid, contact, address, country)
            db.session.add(data_user)
            db.session.commit()

            all_users = User.query.join(Tenant, User.tenantid==Tenant.tenantid).add_columns(User.username, User.password, User.role, User.active, User.emailid, User.contact, User.address, User.country, Tenant.organization_name).order_by(User.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("user_management.html", username=username, all_users=all_users, role=role)

        else:
            db_query_user = db.session.query(User)
            query_user_filter = db_query_user.filter(User.username == username)
            for i in query_user_filter:
                tenantid = i.tenantid
            name = request.form['name'].replace(" ", "")
            password = request.form['password'].replace(" ", "")
            user_role = request.form['user_role']
            active = request.form['active']

            if active == "Active":
                active = True
            else:
                active = False

            format = "%Y-%m-%d %H:%M:%S"
            now_utc = datetime.now(timezone('UTC'))
            now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
            time_created = now_asia.strftime(format)
            now_utc = datetime.now(timezone('UTC'))
            now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
            time_updated = now_asia.strftime(format)
            emailid = request.form['emailid'].replace(" ", "")
            contact = int(request.form['contact'].replace(" ", ""))
            address = request.form['address'].replace(" ", "")
            country = request.form['country'].replace(" ", "")

            query_user_filter = db_query_user.filter(User.emailid == emailid)
            if query_user_filter.count()!=0:
                return "email id already in database"
            query_user_filter = db_query_user.filter(User.contact == contact)
            if query_user_filter.count()!=0:
                return "contact already in database, try using different"
            query_user_filter = db_query_user.filter(User.username == name)
            if query_user_filter.count()!=0:
                return "name already taken"

            #if request.method == 'POST':
                # f = request.files['myphoto']
                #
                # if os.path.exists("static/img/profile_photo/%s" %name):
                #     try:
                #         shutil.rmtree("static/img/profile_photo/%s" %name, ignore_errors=True)
                #     except OSError as e:
                #         print ("Error: %s - %s." % (e.filename, e.strerror))
                # os.mkdir("static/img/profile_photo/%s" %name)
                # file_folder =  "static/img/profile_photo/%s" %name
                # try:
                #     f.save(os.path.join(file_folder, f.filename))
                # except:
                #     print("no photo provided")

            data_user = User(tenantid, name, password, user_role, active, time_created, time_updated, emailid, contact, address, country)
            db.session.add(data_user)
            db.session.commit()

            users = User.query.filter(User.tenantid == tenantid).order_by(User.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("user_management.html", username=username, users=users, role=role)

#####################################################################################################################################
@app.route('/get_client', methods=['POST'])
def get_client():

    global client_organization_name
    global client_contact
    global local_client_search

    got_response = request.form.get("clientdetail")
    got_response = got_response[1:-1]

    client_organization_name = got_response.split(',')[0][1:-1]
    client_realm = got_response.split(',')[1][2:-1]
    client_contact = int(got_response.split(',')[1:][1][10:-2])
    client_country = got_response.split(',')[1:][2][2:-1]
    client_address = ''
    client_address = client_address.join(got_response.split(',')[1:][3:])
    client_address = client_address[2:-1]
    local_client_search = "Not None"

    return render_template("add_client.html", username=username, role=role, client_organization_name=client_organization_name, client_realm=client_realm, client_contact=client_contact, client_address=client_address, client_country=client_country)

@app.route('/add_client/<int:page_num>', methods=['POST'])
def add_client(page_num):
    try:
        global local_client_search
        if local_client_search != None:
            organization_name = request.form['clientname'].replace(" ", "")
            realm = request.form['realm'].replace(" ", "")
            locked = request.form['Locked']

            if locked == "Yes":
                locked = True
            else:
                locked = False

            format = "%Y-%m-%d %H:%M:%S"
            now_utc = datetime.now(timezone('UTC'))
            now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
            time_updated = now_asia.strftime(format)
            contact = int(request.form['contact'].replace(" ", ""))
            address = request.form['address'].replace(" ", "")
            country = request.form['country'].replace(" ", "")
            db_query_tenant = db.session.query(Tenant)

            query_tenant_filter = db_query_tenant.filter(Tenant.organization_name == organization_name)
            if query_tenant_filter.count()!=0 and organization_name != client_organization_name:
                return "client already in database"
            query_tenant_filter = db_query_tenant.filter(Tenant.realm == realm)
            if query_tenant_filter.count()!=0 and realm != client_realm:
                return "realm already in database, give different realm"
            query_tenant_filter = db_query_tenant.filter(Tenant.contact == contact)
            if query_tenant_filter.count()!=0 and contact != client_contact:
                return "contact already in database, try using different"

            # if request.method == 'POST':
            #     f = request.files['logo']
            #     if os.path.exists("static/img/tenant_logo/%s" %organization_name):
            #         try:
            #             shutil.rmtree("static/img/tenant_logo/%s" %organization_name, ignore_errors=True)
            #         except OSError as e:
            #             print ("Error: %s - %s." % (e.filename, e.strerror))
            #     os.mkdir("static/img/tenant_logo/%s" %organization_name)
            #     file_folder =  "static/img/tenant_logo/%s" %organization_name
            #
            #     try:
            #         f.save(os.path.join(file_folder, f.filename))
            #     except:
            #         print("no photo provided")

            query_tenant_filter = db_query_tenant.filter(Tenant.organization_name == client_organization_name).update({'organization_name': organization_name, 'realm':realm, 'locked':locked, 'time_updated':time_updated, 'contact':contact, 'address':address, 'country':country},synchronize_session='fetch')
            db.session.commit()
            print("updated to database")

            local_client_search = None

            all_tenant = Tenant.query.order_by(Tenant.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("client_management.html", username=username, all_tenant=all_tenant, role=role)

        else:
            organization_name = request.form['clientname'].replace(" ", "")
            realm = request.form['realm'].replace(" ", "")
            locked = request.form['Locked']

            if locked == "Yes":
                locked = True
            else:
                locked = False

            format = "%Y-%m-%d %H:%M:%S"
            now_utc = datetime.now(timezone('UTC'))
            now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
            time_created = now_asia.strftime(format)
            now_utc = datetime.now(timezone('UTC'))
            now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
            time_updated = now_asia.strftime(format)
            contact = int(request.form['contact'].replace(" ", ""))
            address = request.form['address'].replace(" ", "")
            country = request.form['country'].replace(" ", "")

            db_query_tenant = db.session.query(Tenant)
            query_tenant_filter = db_query_tenant.filter(Tenant.organization_name == organization_name)
            if query_tenant_filter.count()!=0:
                return "client already in database"
            query_tenant_filter = db_query_tenant.filter(Tenant.realm == realm)
            if query_tenant_filter.count()!=0:
                return "realm already in database, check for appropriate realm"
            query_tenant_filter = db_query_tenant.filter(Tenant.contact == contact)
            if query_tenant_filter.count()!=0:
                return "contact already in database, try using different"

            # if request.method == 'POST':
            #     f = request.files['logo']
            #     if os.path.exists("static/img/tenant_logo/%s" %organization_name):
            #         try:
            #             shutil.rmtree("static/img/tenant_logo/%s" %organization_name, ignore_errors=True)
            #         except OSError as e:
            #             print ("Error: %s - %s." % (e.filename, e.strerror))
            #     os.mkdir("static/img/tenant_logo/%s" %organization_name)
            #     file_folder =  "static/img/tenant_logo/%s" %organization_name
            #
            #     try:
            #         f.save(os.path.join(file_folder, f.filename))
            #     except:
            #         print("no photo provided")

            data_tenant = Tenant(organization_name, realm, locked, time_created, time_updated, contact, address, country)
            db.session.add(data_tenant)
            db.session.commit()

            all_tenant = Tenant.query.order_by(Tenant.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
            return render_template("client_management.html", username=username, all_tenant=all_tenant, role=role)
    except:
        organization_name = request.form['clientname'].replace(" ", "")
        realm = request.form['realm'].replace(" ", "")
        locked = request.form['Locked']

        if locked == "Yes":
            locked = True
        else:
            locked = False

        format = "%Y-%m-%d %H:%M:%S"
        now_utc = datetime.now(timezone('UTC'))
        now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
        time_created = now_asia.strftime(format)
        now_utc = datetime.now(timezone('UTC'))
        now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
        time_updated = now_asia.strftime(format)
        contact = int(request.form['contact'].replace(" ", ""))
        address = request.form['address'].replace(" ", "")
        country = request.form['country'].replace(" ", "")

        db_query_tenant = db.session.query(Tenant)
        query_tenant_filter = db_query_tenant.filter(Tenant.organization_name == organization_name)
        if query_tenant_filter.count()!=0:
            return "client already in database"
        query_tenant_filter = db_query_tenant.filter(Tenant.realm == realm)
        if query_tenant_filter.count()!=0:
            return "realm already in database, check for appropriate realm"

        # if request.method == 'POST':
        #     f = request.files['logo']
        #     if os.path.exists("static/img/tenant_logo/%s" %organization_name):
        #         try:
        #             shutil.rmtree("static/img/tenant_logo/%s" %organization_name, ignore_errors=True)
        #         except OSError as e:
        #             print ("Error: %s - %s." % (e.filename, e.strerror))
        #     os.mkdir("static/img/tenant_logo/%s" %organization_name)
        #     file_folder =  "static/img/tenant_logo/%s" %organization_name
        #
        #     try:
        #         f.save(os.path.join(file_folder, f.filename))
        #     except:
        #         print("no photo provided")

        data_tenant = Tenant(organization_name, realm, locked, time_created, time_updated, contact, address, country)
        db.session.add(data_tenant)
        db.session.commit()

        all_tenant = Tenant.query.order_by(Tenant.time_updated.desc()).paginate(per_page=5, page=page_num, error_out=True)
        return render_template("client_management.html", username=username, all_tenant=all_tenant, role=role)

#############################################################################################################################################
@app.route('/save_profile', methods=['POST'])
def save_profile():
    global username
    name = request.form['name']
    emailid = request.form['email']
    contact = int(request.form['contact'])
    address = request.form['address']
    country = request.form['country']

    if request.method == 'POST':
        f = request.files['myphoto']

        if os.path.exists("static/img/profile_photo/%s" %name):
            try:
                shutil.rmtree("static/img/profile_photo/%s" %name)
            except OSError as e:
                print ("Error: %s - %s." % (e.filename, e.strerror))
        os.mkdir("static/img/profile_photo/%s" %name)
        file_folder =  "static/img/profile_photo/%s" %name
        try:
            f.save(os.path.join(file_folder, f.filename))
        except:
            print("no photo provided")

    db_query_user = db.session.query(User)
    query_user_filter = db_query_user.filter(User.username == username)
    query_user_filter = db_query_user.filter(User.username == username).update({'username':name, 'emailid':emailid, 'contact':contact, 'address':address, 'country':country},synchronize_session=False)
    db.session.commit()
    username = name
    print("done", username)
    return render_template("update_profile.html", username=username, role=role)
#############################################################################################################################################
@app.route('/password_changed', methods=['POST'])
def password_changed():

    emailid = request.form['email']
    password = request.form['password']
    confirm_password = request.form['confirm_password']


    db_query_user = db.session.query(User)
    query_user_filter = db_query_user.filter(User.username == username)
    for i in query_user_filter:
        original_email = i.emailid

    if original_email != emailid:
        query_user_filter = db_query_user.filter(User.emailid == emailid)
        if query_user_filter.count()!=0:
            return "email id already in database"

    query_user_filter = db_query_user.filter(User.username == username).update({'emailid':emailid, 'password':password},synchronize_session=False)
    db.session.commit()

    print(request.referrer)
    if request.referrer == '/submit/1':
        return redirect("/home/1")
    else:
        return redirect(request.referrer)

#####################################################################################################################################
if __name__ == "__main__":
    app.run(host='0.0.0.0',port='5000')
