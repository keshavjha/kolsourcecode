import pandas as pd
from googlesearch import search
import time, os, sys, shutil
import ssl
ssl._create_default_https_context = ssl._create_unverified_context


def make_archive(source, destination):
    base = os.path.basename(destination)
    name = base.split('.')[0]
    format = base.split('.')[1]
    archive_from = os.path.dirname(source)
    archive_to = os.path.basename(source.strip(os.sep))
    print(source, destination, archive_from, archive_to)
    shutil.make_archive(name, format, archive_from, archive_to)
    shutil.move('%s.%s'%(name,format), destination)
    return 0

def arg_str(arg_taken):
    return arg_taken

arg_taken = arg_str(sys.argv[1])
print(arg_taken)

dir_path = os.path.dirname(os.path.realpath(__file__))
inputpath = "%s/%s/input_folder" % (dir_path,arg_taken)
filename=""
for f in os.listdir(inputpath):
    filename = f

df = pd.read_excel("uploads/media/%s/input_folder/%s" %(arg_taken,filename))

for name in df["Name"]:
    if type(name) != float:
        Names = []
        Links = []
        key_words = []
        # var = 0
        for i in df[df["Name"]==name]['Name combination']:
            if type(i) != float:
                for speciality in df[df['Name combination']==i]['Specialty']:
                    if type(speciality) != float:
                        specialty = speciality
                keywords = df[df["Name combination"]==df["Name combination"][0]]['Keywords to be used with all name combinations']
                for new_keyword in keywords:
                    if type(new_keyword) != float:
                        for key in new_keyword.split(","):
                            key=key.replace(" ", "")
                            # if var == 0:
                            query = i+" "+"AND"+" "+specialty+" "+"AND"+" "+key[1:-1]
                            # var = 1
                            try:
                                for link in search(query, tld="com", num=60, stop=60, pause=100):
                                    if link not in Links:
                                        Names.append(name)
                                        Links.append(link)
                                        key_words.append(key[1:-1])
                                    else:
                                        indexer = Links.index(link)
                                        key_words[indexer] = key_words[indexer] + "; " + key[1:-1]
                            except:
                                pass

                                            
        final_df = pd.DataFrame()
        final_df = pd.DataFrame(list(zip(Names, Links, key_words)),
                             columns = ['Name', 'Urls', 'Keywords'])
        
        final_df.to_excel("uploads/media/%s/output_folder/%s.xlsx" %(arg_taken,name), index=False)

        make_archive('uploads/media/%s/output_folder' %(arg_taken), 'uploads/media/%s/zipfolder/output.zip' %(arg_taken))